#! /usr/bin/perl -w
#/*@@
#  @file      FlexIO.pl
#  @date      Tue 21 December 2004
#  @author    Thomas Radke
#  @desc
#             Configures Cactus with the FlexIO library provided by this thorn
#  @enddesc
#  @version   $Header$
#@@*/

# just set a makefile variable telling other thorns that FlexIO is there
print <<'EOF';
BEGIN MAKE_DEFINITION
HAVE_FLEXIO = 1
END MAKE_DEFINITION

INCLUDE_DIRECTORY $(PACKAGE_DIR)/CactusExternal/FlexIO/src
EOF

exit (0);
