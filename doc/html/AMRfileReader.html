<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html>
  <head>
    <title>AmrFileReader</title>
  </head>

  <body bgcolor="#F0F0F0">
   <font face="arial,helvetica"> <h1>AmrFileReader</h1></font>

    The <a href="AmrGridReader">AmrGridReader</a> only provides
    unfiltered access to the grids stored in the AMR datafile.  For a
    real visualization application, it is necessary to filter through
    these grids to present only what is relevant for a particular
    domain and timestep.  The <b>AmrFileReader</b> provides that sort
    of filtering capability.<p>
      The <b>AmrFileReader</b> selects all grids that are active at a
      particular timestep in the evolution sequence.  It also allows
      you to mask out sets of levels (for example only show levels 1-3 
      of a 7 level hierarchy).  Since the stepping of grids proceeds
      at different paces at different levels, the timestep is a global 
      quantity that refers to the finest granularity of stepping that
      can occur during the evolution (the integer timesteps of the
      deepest level of refinement).  The API is intelligent enough to
      only reload data from grids that have changed when a different
      timestep has been selected since deeper-level grids will change
      more rapidly than their parent grids.<p>
    <hr>
       <font face="arial,helvetica" color="#555588"><h2>The API</h2></font>
    <UL>
      <LI><a href="#CPP">C++ Interface</a>
      <LI><a href="#C">C Interface</a>
      <LI><a href="#F77">F77/F90 Interface</a>
    </UL>
    <hr>
    <u><h2><a name="CPP">C++ Interface</a></h2></u>
    <DL>
      <DT><font color="#888888"><b>Constructors/Destructors</b></font>
    <DL>
      <DT><b>AmrFileReader::AmrFileReader(IObase
	  &filehandle)</b> <i>constructor</i>
      <DD>The <i>filehandle</i> is an already
	<a href="UsingC++.html#Opening">open IEEEIO or HDFIO
	  file</a>.  This initializes the reader and attaches it to that
	datafile.
      <DT><b>AmrFileReader::~AmrFileReader()</b> <i>destructor</i>
      <DD>This flushes all of the necessary file buffers.
	The filehandle that the reader was bound to must be
	closed separately though.
    </DL>
    <DT><font color="#888888"><b>Queries about File Contents</b></font>
    <DL>
      <DT><b>int AmrFileReader::nLevels()</b>
      <DD>Returns the number of levels in the AMR hierarchy.  This is
	the deepest the hierarchy gets in this particular datafile as
	opposed to the number of levels present at this time in the
	evolution (this distinction is important for adaptive depth
	schemes where the number of levels will change depending on
	the demands of the solver).
      <DT><b>AmrFileReader::getTimeRange(int &mintime,int &maxtime)</b>
      <DD>Gets the range of integer timesteps stored in the file.
	The nonzero mintime exists to support schemes where the
	evolution is broken up among several files.
    </DL>
    <DT><font color="#888888"><b>Filters</b></font>
      <DT>The <i><b>filter</b></i> methods listed below select grids based on 
	the criteria of time and level number.  Grids selected by the
	filter are referred to as <i><b>active grids</b></i>.<p>
    <DL>
      <DT><b>AmrFileReader::setTime(int time)</b>
      <DD>Sets the current timestep to select the data hierarchy for.
	The timestep is defined with respect to the rate at which
	grids change at the deepest level of the AMR hierarchy stored
	in the file.  Selecting a particular timestep selects a
	snapshot of the hieararchy at a particular time in the
	evolution.
	<DT><b>AmrFileReader::hideLevel(int level)</b>
	<DD>By default all levels are visible and are read in when the 
	<code>getGrids()</code> method is called.  You can hide
	individual levels using this method.  This is useful when
	loading the entire hierarchy is either too confusing, too slow, or too
	large for the workstation to handle.
	<DT><b>AmrFileReader::showLevel(int level)</b>
	<DD>Shows a level that has otherwise been hidden.
	<DT><b>AmrFileReader::showAllLevels()</b>
	<DD>A convenience method for making all of the levels visible.
    </DL>
     <DT><font color="#888888"><b>Getting the Data</b></font>
      <DL>	
      <DT><b>int AmrFileReader::getGrids(FlexArray&ltAmrGrid&gt &g)</b>
      <DD>Gets all currently active grids and puts them into a
	FlexArray of 
	<a href="AmrGrid.html">AmrGrid</a> datastructures. An
	<i>active grid</i> is one that is currently selected by the
	<code>setTime()</code> and <code>show/hideLevel()</code> filters.  The
	FlexArray is a simple template class for arrays that grow
	automatically to accomodate new elements.  If you or your
	compiler do not like templates, then you can use the two-step
	static allocation method
	(<code>getNumGrids()</code>/<code>getGrids()</code>) defined
	below.
	This method returns the number of active grids.
      <DT><b>int AmrFileReader::getNumGrids()</b>
      <DD>Returns the total number of active grids currently selected
	by the 	<code>setTime()</code> and
	<code>show/hideLevel()</code> 
	filters.  This is used to determine how 
	large of an array of AmrGrid structures to allocate in order
	to receive the data using the <code>getGrids()</code> method
	call since this array length is static.
      <DD><b>int AmrFileReader::getGrids(AmrGrid *grids)</b>
      <DD>This method reads the grid information and data for all
	currently active grids into the pre-allocated array of 
	<code>grids</code> passed to it.  The API will allocate memory 
	for the grids upon reading the data, but it is the responsibility of
	the programmer to dispose of the array of grids and the data
	stored within these datastructures once they are done with it.
    </DL>
    <DT><font color="#888888"><b>Utility and Debugging Methods</b></font>
      <DT><DL>
      <DT><b>AmrFileReader::setDataLoadingOff()</b>   
      <DD>This causes the <code>getGrids()</code> methods
	to read in the informational part of 
	the AmrGrid datastructures, but not load any data.  This is
	useful for just aquiring bounding box information or
	estimating the memory/computational requirements for loading
	the entire hierarchy.
      <DT><b>AmrFileReader::setDataLoadingOn()</b>   
      <DD>This causes the <code>getGrids()</code> methods
	to read both the informational and data part of 
	the AmrGrid datastructures.  This is the default state for
	this API.
      <DT><b>printGridInfo()</b>
      <DD>Prints information about all grids in file to STDERR.  Used
	only for debugging.
      <DT><b>printActiveGrids()</b>
      <DD>Prints information about all <b>active</b> grids to STDERR.
	Used only for debugging.
	<DT><b>int debug</b>
	This is a public variable that can be set to a nonzero value
	in order to print additional debugging information for each
	method call.  This printing is turned off when it is set to zero.
    </DL>
  </DL>
    <hr>
    <h2><u><a name="C">C Interface</a></u></h2>
    Not ready yet.
    <hr>
    <h2><u><a name="F77">F77/F90 Interface</a></u></h2>
    Not ready yet.
    <hr>
    <address><a href="mailto:jshalf@ariel.ncsa.uiuc.edu"></a></address>
<!-- Created: Thu Feb  4 10:03:12 CST 1999 -->
<!-- hhmts start -->
Last modified: Thu Feb  4 17:49:33 CST 1999
<!-- hhmts end -->
  </body>
</html>
