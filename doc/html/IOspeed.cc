#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/times.h>
#include <time.h>
#include "IO.hh"
#include "IEEEIO.hh"
#include "HDFIO.hh"
extern "C" {
void openf77_();
void writef77_(double *data);
void closef77_();
}

void main(){
  double data[64*64*64];
  int dims[3]={64,64,64};
  int i,nds;
  puts("--------------IEEE--------------");
  for(i=0,nds=5;i<8;i++,nds+=5){
    time_t stm,etm;
    struct tms stms,etms;
    time(&stm);
    times(&stms);
    IObase *file = new IEEEIO("speed.raw",IObase::Write);
    fprintf(stderr,"Write %2u:",nds);
    for(int n=0;n<nds;n++){
      fprintf(stderr,"*");
      file->write(IObase::Float64,3,dims,data);
    }
    delete file;
    times(&etms);
    time(&etm);
    printf("\n\tRealtime=%d, UserTime=%d, SystemTime=%d, Combined=%u\n",
	   etm-stm,
	   etms.tms_utime-stms.tms_utime,
	   etms.tms_stime-stms.tms_stime,
	   etms.tms_utime-stms.tms_utime+etms.tms_stime-stms.tms_stime);
  }

  puts("--------------HDF---------------");
  for(i=0,nds=5;i<8;i++,nds+=5){
    time_t stm,etm;
    struct tms stms,etms;
    time(&stm);
    times(&stms);
    IObase *file = new HDFIO("speed.hdf",IObase::Write);
    fprintf(stderr,"Write %2u:",nds);
    for(int n=0;n<nds;n++){
      fprintf(stderr,"*");
      file->write(IObase::Float64,3,dims,data);
    }
    delete file;
    times(&etms);
    time(&etm);
    printf("\n\tRealtime=%d, UserTime=%d, SystemTime=%d, Combined=%u\n",
	   etm-stm,
	   etms.tms_utime-stms.tms_utime,
	   etms.tms_stime-stms.tms_stime,
	   etms.tms_utime-stms.tms_utime+etms.tms_stime-stms.tms_stime);
  }

  puts("-------------F77 Unformatted---------------");
  for(i=0,nds=5;i<8;i++,nds+=5){
    time_t stm,etm;
    struct tms stms,etms;
    time(&stm);
    times(&stms);
    fprintf(stderr,"Write %2u:",nds);
    openf77_();
    // writef77_(dat,&nds);
    for(int n=0;n<nds;n++){
      fprintf(stderr,"*");
      writef77_(data);
    }
    closef77_();
    times(&etms);
    time(&etm);
    printf("\n\tRealtime=%d, UserTime=%d, SystemTime=%d, Combined=%u\n",
           etm-stm,
           etms.tms_utime-stms.tms_utime,
           etms.tms_stime-stms.tms_stime,
           etms.tms_utime-stms.tms_utime+etms.tms_stime-stms.tms_stime);
  }
}

