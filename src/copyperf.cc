#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "vatoi.hh"
#include "Timer.hh"

int main(int argc,char *argv[]){
  // compare speed of bcopy to character loop
  if(argc<2){
    puts("need to define buffer length");
    exit(0);
  }
  long size = vatoi(argv[1]);
  int ntimes = 1;
  register int n;
  register long i;
  if(argc>2)
    ntimes = vatoi(argv[2]);
  printf("Buffer is %lu bytes %u times\n",size,ntimes);
  Timer t1,t2;
  char *dst = new char[size];
  char *src = new char[size];
  // prime the caches
  for(i=0;i<size;i++){
    src[i]=(char)(i%255);
    dst[i]=0;
  }
  t1.start();
  for(n=0;n<ntimes;n++) 
    for(i=0;i<size;i++) 
      dst[i]=src[i];
  t1.stop();
  t2.start();
  for(n=0;n<ntimes;n++)
    bcopy(src,dst,size);
  t2.stop();
  t1.print("Std copy timing: ");
  t2.print("bcopy timing: ");
  delete src;
  delete dst;
  return 1;
}
