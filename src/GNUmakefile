#
#  FlexIO Interface to the Makefile Configuration System, as
#  described and available at
#
#	http://amira.zib.de/make/
#
#   $Revision: 1.2 $
#   $Date: 2004-08-30 16:55:16 $
#   $Log: not supported by cvs2svn $
#   Revision 1.6  2003/07/16 14:46:47  werner
#   Adjusted flags to make optional use of an external HDF5Open call, which is
#   bundled into an `hdf5wrap' library. It is used in the context of Amira to
#   enable globus/remote HDF5 for an existing binary without recompiling.
#
#   Revision 1.5  2003/02/13 21:14:49  werner
#   Adjusted makefile to new Amira CVS structure. Doesn't influence anyone else.
#
#   Revision 1.4  2000/05/29 14:06:11  werner
#   Using VPATH instead of PWD is much saver.
#
#   Revision 1.3  2000/05/25 16:20:38  werner
#   HDF4 interface enabled
#
#   Revision 1.2  2000/05/17 10:45:38  werner
#   GNUmakefile falls back to include the Makefile in the same directory, if
#   no ../GNUmakefile.rules exists.
#
#   Revision 1.1  2000/05/10 11:36:52  werner
#   A GNUmakefile to enable compilation of FlexIO directly in the context of
#   Amira. Please cry if this leads to problems; if so, we have to find some
#   more complicated mechanism...
#
#
#

OBJECTS = IO.$O IEEEIO.$O FlexIO.$O

HDF4OBJECTS = HDFIO.$O
HDF5OBJECTS = H5IO.$O 

AMROBJECTS = AmrGridReader.$O AmrUcdFileReader.$O \
        AmrFileReader.$O Bounds.$O \
        AmrUcdGridHierarchy.$O AMRwriter.$O \
	Writer.$O

SOCKOBJECTS = SockIOreader.$O SockIOwriter.$O
MPOBJECTS = MPIO.$O MPIutils.$O

OBJS    = $(OBJECTS) $(AMROBJECTS) $(HDF5OBJECTS_${HDF5}) $(HDF4OBJECTS_${HDF4})

HDF4OBJECTS_true=$(HDF4OBJECTS)
HDF5OBJECTS_true=$(HDF5OBJECTS)

TARGET  = FlexIO
PACKAGE = $(TARGET)

all.dtor=remove-config visible

RULEFILE = $(shell if [ -r $(VPATH)../GNUmakefile.rules ] ; then echo $(VPATH)../GNUmakefile.rules ; else echo Makefile ; fi)

include $(RULEFILE)

CXXFLAGS += $(hdf5_FLAGS) $(hdf4_FLAGS) -I$(VPATH)./
LIBS     += $(hdf5_LIB)   $(hdf4_LIB) -lm $(hdf5wrap_LIB)

#
# Add a variable to the makefile configuration
#
ifdef	AMIRA_NUMREL
TARGET_FLAGS=-I$$(MAKE_LOCAL)/src/numrel/FlexIO -I$$(MAKE_ROOT)/src/numrel/FlexIO $(hdf5_FLAGS) $(hdf4_FLAGS)
else
TARGET_FLAGS=-I$$(MAKE_LOCAL)/external/FlexIO -I$$(MAKE_ROOT)/external/FlexIO $(hdf5_FLAGS) $(hdf4_FLAGS)
endif

