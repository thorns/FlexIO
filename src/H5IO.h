#ifndef __H5IO_H_
#define __H5IO_H_

#include "IOProtos.h"
#include "Arch.h"

IOFile H5IOopen PROTO((char *filename,char *accessname));
IOFile H5IOopenRead PROTO((char *filename));
IOFile H5IOopenWrite PROTO((char *filename));
IOFile H5IOopenAppend PROTO((char *filename));

#endif
