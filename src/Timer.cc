#include <unistd.h>
#include "Timer.hh"
#include <limits.h>
int Timer::start(){
  // struct timezone tz;
  if(running) return 0; // already running
  running=1;
  times(&tm); // set current time 
  gettimeofday(&tv,0);
  return 1;
}

int Timer::stop(){
  // timezone tz;
  tms tmc; // current time
  timeval tvc; // current realtime
  if(!running) return 0; // already stopped
  running=0;
  // copy back current time
  gettimeofday(&tvc,0);
  times(&tmc);
  treal += ((double)(tvc.tv_sec - tv.tv_sec) + 
	    ((double)(tvc.tv_usec - tv.tv_usec))/1000000.0L);
  tuser += (double)(tmc.tms_utime-tm.tms_utime)/CLK_TCK;
  tsystem += (double)(tmc.tms_stime-tm.tms_stime)/CLK_TCK;
  return 1;
}

void Timer::elapsedTimeSeconds(double &system,double &user,double &real){
  int wasrunning=running;
  stop();
  system=tsystem;
  user=tuser;
  real=treal;
  if(wasrunning) start();
}

void Timer::print(char *preface,FILE *f){
  double s,u,r;
  elapsedTimeSeconds(s,u,r);
  fprintf(f,"%s: SystemTime=%lfs\tUserTime=%lfs\tRealTime=%lfs\n",
	  preface,s,u,r);
}

