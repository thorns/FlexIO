/*  
 *  This example writes data to the HDF5 file.
 *  Data conversion is performed during write operation.  
 */
// #include <H5IO.hh>

/* Some macros to fix compatibility issues as long as 1.8.0 is in beta
   phase */
#define H5_USE_16_API 1
#include <hdf5.h>
#define WRITE_H5
#define FILE        "SDS.h5"
#define DATASETNAME "IntArray" 
#define NX     5                      /* dataset dimensions */
#define NY     6
#define RANK   2
/*
Perhaps support a filtration/pattern matching architecture which
recognizes particular patterns of values in the file and offers those
patterns as a new typename in the architecture or even a particular
callback.  So for instance you could have a group of "dataset" with
"edgecoords" is a rectilinear dataset.  Otherwise we'll need to 
match groupnames?  Could have a "type" attribute which disambiguates
this because each object must have a unique text name.
 */

herr_t GroupOp(hid_t group_id,
	       const char *member_name, 
	       void *operator_data){
  hid_t dataset,space,datatype;
  hsize_t rank, dims[5];
  H5T_class_t typeclass;
  size_t typesize;
  int i;
  
  printf("GroupOp: member_name = %s id %u\n",member_name,group_id);
  /* get info on a particular member */
  dataset = H5Dopen(group_id,member_name);
  space = H5Dget_space(dataset);
  datatype = H5Dget_type(dataset);
  rank = H5Sget_simple_extent_ndims(space);
  H5Sget_simple_extent_dims(space,dims,NULL); 
  printf("\tndims=%d Dims ",rank);
  for(i=0;i<rank;i++)
    printf("%u ",dims[i]);
  puts("");
  
  typeclass = H5Tget_class(datatype);
  typesize  = H5Tget_size(datatype);

  printf("\tNbytes=%u Type = ",typesize);
  switch(typeclass){
  case H5T_INTEGER:
    puts("Int");
    break;
  case H5T_FLOAT: 
    puts("Float");
    break;
  case H5T_TIME:
    puts("Time");
    break;
  case H5T_STRING:
    puts("String");
    break;
  case H5T_BITFIELD:
    puts("Bitfield");
    break;
  case H5T_OPAQUE:
    puts("Opaque");
    break;
  case H5T_COMPOUND: 
  default:
    puts("Unknown");
    break;
  }

  H5Sclose(space);
  H5Tclose(datatype);
  H5Dclose(dataset);
  return 0;
}

main (void)
{
    hid_t       file, dataset;         /* file and dataset handles */
    hid_t       datatype, dataspace;   /* handles */
    hsize_t     dimsf[2];              /* dataset dimensions */
    herr_t      status;                             
    int         data[NX][NY];          /* data to write */
    int         i, j;
    int idx,rval;

    /* 
     * Data  and output buffer initialization. 
     */
    for (j = 0; j < NX; j++) {
        for (i = 0; i < NY; i++)
            data[j][i] = i + j;
    }     
    /*
     * 0 1 2 3 4 5 
     * 1 2 3 4 5 6
     * 2 3 4 5 6 7
     * 3 4 5 6 7 8
     * 4 5 6 7 8 9
     */

    /*
     * Create a new file using H5F_ACC_TRUNC access,
     * default file creation properties, and default file
     * access properties.
     */
#ifdef WRITE_H5
    file = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

/*
     * Describe the size of the array and create the data space for fixed
     * size dataset. 
     */
    dimsf[0] = NX;
    dimsf[1] = NY;
    dataspace = H5Screate_simple(RANK, dimsf, NULL); 

    /* 
     * Define datatype for the data in the file.
     * We will store little endian INT numbers.
     */
    datatype = H5Tcopy(H5T_NATIVE_INT);
    status = H5Tset_order(datatype, H5T_ORDER_LE);

    /*
     * Create a new dataset within the file using defined dataspace and
     * datatype and default dataset creation properties.
     */
    dataset = H5Dcreate(file, DATASETNAME, datatype, dataspace,
                        H5P_DEFAULT);

    /*
     * Write the data to the dataset using default transfer properties.
     */
    status = H5Dwrite(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL,
                      H5P_DEFAULT, data);
    H5Dclose(dataset);

    // writing second ------------------------------------------
    dataset = H5Dcreate(file, "anothername", datatype, dataspace,
                        H5P_DEFAULT);

    /*
     * Write the data to the dataset using default transfer properties.
     */
    status = H5Dwrite(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL,
                      H5P_DEFAULT, data);

    /*
     * Close/release resources.
     */
    H5Sclose(dataspace);
    H5Tclose(datatype);
    H5Dclose(dataset);
    H5Fclose(file);
#endif
    /* typedef herr_t *(H5G_operator_t)(hid_t group_id, 
                       const char *member_name, 
		       void *operator_data     // in,out); */
    file = H5Fopen(FILE, H5F_ACC_RDONLY, H5P_DEFAULT);
    printf("FileID=%u\n",file);
    idx=0;
   
    rval=H5Giterate(file, /* hid_t loc_id, */
		    "/", /*const char *name, */
		    &idx, /* int *idx, */
		    GroupOp, 
		    NULL);
    printf("Iteration info was %u  rval=%d\n",idx,rval);
    puts("done");
    H5Fclose(file);
    return 0;
}
