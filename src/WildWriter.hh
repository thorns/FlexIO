#ifndef __WILDWRITER_HH_
#define __WILDWRITER_HH_

#include "IO.hh"
#include "IEEEIO.hh"
#include "FlexArrayTmpl.H"

#define DISKBLOCKSIZE 8192
#define BLOCKSIZE 2*DISKBLOCKSIZE

// Does not assume contiguous data (writes in chunks)
class WildWriterB {
  IObase &file;
  int nnodes,ncells,ncomp;
  char buffer[BLOCKSIZE]; // could dynamically allocate
  FlexArray<char> compnames;
  int totalcomp;
  IObase::DataType comptype;
  FlexArray<int> compsizes;
  int bufferindex,masterindex,segsize;
public:
  WildWriterB(IObase &descriptor):file(descriptor){
  }
  ~WildWriterB(){}
  void beginDataset(int numnodes,
		    int numcells,
		    IObase::DataType datacomponenttype,
		    int numdatacomponents,
		    int *componentsizes,
		    char *componentnames,
		    char componentnameseparator);

  void beginNodes(int numnodes);
  void beginNodes();
  inline void addNode(float *coords){
    // attrib WildNode[%u] (should buffer internally)
    // but we can only chunk on Datasets :(
    // so collect data in 8k buffers
    //checkMode(WriteNodes);
    float *copyptr=(float *)buffer;
    // copy into master buffer.
    int index=bufferindex*3;
    for(int i=0;i<3;i++) copyptr[index+i]=coords[i];
    bufferindex++;
    masterindex++;
    if(bufferindex>=segsize){ // dump the buffer to disk
      int origin[2]={0,0},dims[2]={3,0};
      dims[1]=segsize;
      origin[1]=masterindex-segsize;
      file.writeChunk(dims,origin,buffer);
      bufferindex=0; // reset the bufferindex for next seg
    }
  }
  void endNodes();
  void beginCells(int numcells);
  void beginCells();
  inline void addCell(int level,int *neighborlist){
    //    checkMode(WriteCells);
    int *copyptr=(int *)buffer;
    int index=bufferindex*9;
    // copy into master buffer.
    copyptr[index+1]=level;
    for(int i=0;i<8;i++) 
      copyptr[index+i+1]=neighborlist[i];
    bufferindex++;
    masterindex++;
    if(bufferindex>=segsize){ // dump the buffer to disk
      int origin[2]={0,0},dims[2]={3,0};
      dims[1]=segsize;
      origin[1]=masterindex-segsize;
      file.writeChunk(dims,origin,buffer);
      bufferindex=0; // reset the bufferindex for next seg
    }
  }
  void endCells();
  void beginData();
  void beginData(IObase::DataType datacomponenttype,
		    int numdatacomponents,
		    int *componentsizes,
		    char *componentnames,
		    char componentnameseparator);
  inline void addData(void *data){
    //    checkMode(WriteCells);
    if(comptype==IObase::Float32){
      float *copyptr=(float *)buffer;
      float *dptr=(float*)data;
      int index = bufferindex*totalcomp*sizeof(double);
      for(int i=0;i<totalcomp;i++) copyptr[index+i]=dptr[i];
    }
    else if(comptype==IObase::Float64){
      double *copyptr=(double *)buffer;
      double *dptr = (double *)data;
      int index = bufferindex*totalcomp*sizeof(double);
      for(int i=0;i<totalcomp;i++) copyptr[index+i]=dptr[i];
    }
    bufferindex++;
    masterindex++;
    if(bufferindex>=segsize){ // dump the buffer to disk
      int origin[2]={0,0},dims[2]={0,0};
      dims[0]=totalcomp;
      dims[1]=segsize;
      origin[1]=masterindex-segsize;
      file.writeChunk(dims,origin,buffer);
      bufferindex=0; // reset the bufferindex for next seg
    }
  }
  void endData();
  void endDataset() ;
};

extern "C" {
#include "WildWriter.h"
}

#endif

/*
// Assumes Contiguous Data
class WildWriterA {
  IO &file;
public:
  WildWriter(IO &descriptor):file(descriptor){
  }
  ~WildWriter(){
  }
  // All nodes for all levels
  void writeNodes(int numnodes,float *node_coords){
    int dims[2]={3,0};
    dims[1]=numnodes;
    file.write(IObase::Float32,2,dims,node_coords);
    file.writeAttribute("WildNodes",IObase::Int,0,dims);
  }
  // Must Be Hexahedral Cells
  void writeCells(int level,int ncells,int *celllist){
    
  }
  // Assumes Node-centered data
  void writeData(char *name,IObase::DataType type,int veclen,void *data){
    
  }
};
*/
