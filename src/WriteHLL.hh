#ifndef __HLL_WRITER_HH_
#define __HLL_WRITER_HH_
#include "IO.hh"

class WriteHLL {
  IObase &file;
  int current_npoints,current_cellsize,current_ndims;
  char *current_celltype;
  IObase::DataType coordtype,indextype;
public:
  WriteHLL(IObase &iofile):file(iofile),current_npoints(0),
  coordtype(IObase::Float32),indextype(IObase::Int32){}
  ~WriteHLL(){}
  // declare a new dataset
  void newDataset(int ndims,long npoints);
  void setCoordType(IObase::DataType dt){ coordtype=dt; }
  void setIndexType(IObase::DataType dt){ indextype=dt; }
  // each array is npoints long
  void writeCoords(void *xcoords,void *ycoords=0,void *zcoords=0); // for 3D
  void writeCoordsXYZ(void *xcoords); // for Packed Coords (any dimension)
  // will assume ncells=npoints unless overridden.
  void writeConnectivity(void *cells,long ncells=-1); // implicit cellsize
  void writeConnectivity(int cellsize,void *cells,long ncells=-1);
  // npoints data points since the data is vertex-centered
  // each dataset can be of independent type
  void writeData(char *dataname,IObase::DataType datatype,
		 void *data,int veclen=1);
};

class ReadHLL {
  IObase &file;
  int current_npoints,current_ncells,current_cellsize,current_ndims;
public:
  ReadHLL(IObase &iofile):
    file(iofile),current_npoints(0),current_ncells(0),
    current_cellsize(0),current_ndims(0){}
  ~ReadHLL(){}
  // declare a new dataset
  void selectDataset(int index,int &ndims,int &npoints,int &ncells,int &ndata);
  // each array is npoints long
  void readDataInfo(char *names[],IObase::DataType *datatypes,int *veclens);
  void readDataInfo(char *names[],int *datatypes,int *veclens);
  void readCoords(float *xcoords,float *ycoords=0,float *zcoords=0);
  void readConnectivity(int *cells);
  // npoints data points since the data is vertex-centered
  // each dataset can be of independent type
  void readData(char *name,void *data);
};
extern "C" {
#include "WriteHLL.h"
	   }
#endif
