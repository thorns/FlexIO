/* *****************************************/
/* Includes and Global Defs                */
/* *****************************************/
#include <stdio.h>
#include <math.h>
/* IAC CODE CHANGE : #include <math.h> */
#include <avs/avs_math.h>

#include <avs/avs.h>
#include <avs/port.h>
#include <avs/field.h>
#include <avs/ucd_defs.h>
#include <stdlib.h>
#include <string.h>
/*#include "sds.h"*/

typedef union Coord {
  struct {
    int x,y,z;
  } axis;
  int vect[3];
} Coord;

int debug=0;

int amrucd_desc (){
  int in_port, out_port, param, iresult;
  extern int amrucd_compute();
  extern int amrucd_destroy();
  /*extern int amrucd_destroy();*/
  
  AVSset_module_name("AMR-UCD Reader", MODULE_DATA); 

  /* Output Port Specifications              */
  out_port = AVScreate_output_port("Output", "ucd" );
  /* out_port = AVScreate_output_port("output","field uniform");*/
  /* Parameter Specifications                */
  

  param = AVSadd_parameter("Filename", "string", "", "", ":");
  AVSconnect_widget(param, "browser");

  param = AVSadd_parameter("Hold","boolean",0,0,1);
  AVSconnect_widget(param,"toggle");

  param = AVSadd_parameter("Threshold","boolean",1,0,1);
  AVSconnect_widget(param,"toggle");

  param = AVSadd_parameter("Generate Level Data","boolean",1,0,1);
  AVSconnect_widget(param,"toggle");
  
  param = AVSadd_parameter("Step", "integer", 0, 0, 1);
  AVSconnect_widget(param, "idial");
  
  param = AVSadd_float_parameter("Min",0.0,FLOAT_UNBOUND,FLOAT_UNBOUND);
  param = AVSadd_float_parameter("Max",1.0,FLOAT_UNBOUND,FLOAT_UNBOUND);
  
  param = AVSadd_parameter("VisibleLevels","choice","<no data>","<no data>",":");
  AVSconnect_widget(param,"choice");
  
  AVSset_compute_proc(amrucd_compute); 
  AVSset_destroy_proc(amrucd_destroy);
  
  return 1;
}

AVSinit_modules()
{
  AVSmodule_from_desc (amrucd_desc);
}
