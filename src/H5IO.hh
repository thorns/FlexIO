#ifndef __H5IO_HH_
#define __H5IO_HH_
#include "Arch.h"
#include "IO.hh"

/*
H5stream for networking & message passing!
This is not the same as HDF5 because you cannot ask for a subset of the
data that arrives.  You can only ask what is in the pipe and recieve it or
dump it. This is only for marshalling and unmarshalling datastructures
into a network pipe for transport in a platform independent manner (ie.
use the HDF5 data translation mechanisms)

Should have pluggable transport layer!
Just provide a VFT with read/write/connect/terminate
     perhaps block/nonblock/and poll for connections too?
     If we can make do with blocking that should be sufficient
     Also need an exception handling mechanism in the embeded/pluggable
     transport.

Transport policies should also be pluggable.  They would be
* periodic (timer-based)
* synchronous/rendezvous (MPI or Occam-like)
* transport on write (active message push)
* transport on read (on-demand-based active message request w/ round-trip)
* virtual shared memory background async transport (implicit sync on read or sync on write, or explicit sync or no-sync)

Query Policy
* Blocking
* nonblocking

Buffer Policy
* WriteBufferSize
* ReadBufferSize

 */

/* Some macros to fix compatibility issues as long as 1.8.0 is in beta
   phase */
#define H5_USE_16_API 1
#include <hdf5.h>

class H5IO : public IObase {
  // *** Flags for usage of each type 
  char filevalid,datasetvalid,datatypevalid,dataspacevalid;
  IObase::DataType currentdatatype;
  hid_t       file, dataset;         /* file and dataset handles */
  hid_t       datatype, dataspace;   /* handles */
  hsize_t     dimsf[5],rankf;              /* current dataset dimensions */
  herr_t      status;                      

  hid_t DataType2H5(IObase::DataType nt); // in-memory type is *always* native
  // IObase::DataType H52DataType(hid_t &nt);
  IObase::DataType H5DataType2DataType(hid_t nt);
  int createdataspace(int rank,CONST int *dims);
  int createdatatype(IObase::DataType dt);
  int selectdataset(int i);
  //  void selectdataset(char *name);
  int createdataset(char *name,IObase::DataType nt,
		    int rank,CONST int *dims);
  int createdataset(IObase::DataType nt,int rank,CONST int *dims);
  int getdatasetinfo(int &rank, int *dims,
		      IObase::DataType &nt);
  void enddataspace();
  void enddatatype(); 
  void enddataset();
  int getndatasets();
  void getdatasetname(int _index, // in
			 char *name);
  
  
  int chunkdims[5];
  int hasread;
public:
  H5IO(CONST char *fname,AccessMode access);
  virtual ~H5IO();
  virtual int isValid();
  virtual int write(IObase::DataType typeID,int rank,CONST int *dims,const void *data); 
  // virtual int write(char *name,IObase::DataType typeID,int rank,CONST int *dims,void *data);
  virtual int readInfo(IObase::DataType &typeID,int &rank,int *dims,int maxdims=3);
  virtual int readInfo(char *name,IObase::DataType &typeID,int &rank,int *dims,int maxdims=3);
  virtual int read(void *data);
  virtual int seek(int i);
  virtual int nDatasets();
  virtual int writeAnnotation(CONST char *annotation);
  virtual int readAnnotationInfo(int number,int &length); // NEW
  virtual int readAnnotation(int index,char *annotation,int maxlen); // changed
  virtual int nAnnotations(); // New

  virtual int writeAttribute(CONST char *name,IObase::DataType typeID,Long length,const void *data); // New
  virtual int readAttributeInfo(int number,char *name,IObase::DataType &typeID,Long &nelem,int maxnamelen=128); // New
  virtual int readAttributeInfo(CONST char *name,IObase::DataType &typeID,Long &nelem); // New
  virtual int readAttribute(int number,void *data); // New
  // virtual Long readAttribute(CONST char *name,void *data);
  virtual int nAttributes(); // New
  
  //-----------------Chunking Utilities..................
  virtual int reserveChunk(IObase::DataType typeID,int rank,CONST int *dims);
  virtual int writeChunk(CONST int *chunkdims,CONST int *chunkorigin,const void *data);
  virtual int readChunk(CONST int *chunkdims,CONST int *chunkorigin,void *data);
  //-----------------Streaming interface (for PANDA, Sockets & MPIO).........
  virtual int reserveStream(IObase::DataType typeID,int rank,CONST int *dims) {return -1; }
  virtual int writeStream(const void *data,int length){ return -1; } // not implemented yet
  virtual int readStream(void *data,int length) { return -1; } // not implemented yet
  //----------------Special HDF5-specific methods.........
  // will put some hyperslab definition stuff here
};

#define f_h5_open F77NAME(h5_open_,h5_open,H5_OPEN)
#define f_h5_openr F77NAME(h5_openr_,h5_openr,H5_OPENR)
#define f_h5_openw F77NAME(h5_openw_,h5_openw,H5_OPENW)
#define f_h5_opena F77NAME(h5_opena_,h5_opena,H5_OPENA)

extern "C" {
#if defined(CRAY) && defined(T3E) // Note: This isn't really implemented yet...
#include <fortran.h>
  Long8 f_h5_open(_fcd fcfilename,_fcd accessname);
  Long8 f_h5_openr(_fcd fcfilename);
  Long8 f_h5_openw(_fcd fcfilename);
  Long8 f_h5_opena(_fcd fcfilename);
#else
  Long8 f_h5_open(char *file,char *access,int flen,int alen);
  Long8 f_h5_openr(char *file,int flen);
  Long8 f_h5_openw(char *file,int flen);
  Long8 f_h5_opena(char *file,int flen);
#endif
#include "H5IO.h"
}
/*
  long io_open_ieee_(constCONST char *file,constCONST char *access,int flen,int alen);
*/

#endif

