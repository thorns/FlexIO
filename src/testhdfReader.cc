#include <stdio.h>
#include <stdlib.h>
#include "IO.hh"
#include "HDFIO.hh"

int main(int argc,char *argv[]){
  float data[40*40*40];
  double origin[3]={0,0,0};
  double dx[3]={0.25,0.25,0.25};
  int i;
  int rank=3;
  int dims[3]={40,40,40};
  IObase *infile;
  if(argc>1)
    infile = new HDFIO(argv[1],IObase::Read);
  else
    infile = new HDFIO("data3d.raw",IObase::Read);
  // reader = new Reader(*infile);
  int ndatasets=infile->nDatasets();
  printf("Number of datasets=%d\n",ndatasets);
  for(i=0;i<ndatasets;i++){
    int j;
    float buffer[40*40*40];
    for(j=0;j<40*40*40;j++) buffer[j]=0;
    int rank,dims[3];
    IObase::DataType datatype;

    printf("Cycle %u\n",i);
    infile->readInfo(datatype,rank,dims);
    printf("Data info dt=%u,rank=%u,dims=%u,%u,%u\n",
	   (int)(datatype),rank,dims[0],dims[1],dims[2]);
    infile->read(buffer);
    for(j=0;j<10;j++)
      printf("Data[%u]=%f\n",j,buffer[j]);
    puts("***done printing data");
    int nattribs=infile->nAttributes();
    printf("Number of Attributes=%d\n",nattribs);
    for(j=0;j<nattribs;j++){
      char attribname[64];
      int length;
      infile->readAttributeInfo(j,attribname,datatype,length);
      printf("\tAttribute[%u] name=%s\n",j,attribname);
      switch(datatype){
      case IObase::Float32:{
	//float buf[5];
	puts("Float");
	printf("length=%u\n",length);
      } break;
      case IObase::Float64:{
	double buf[5];
	puts("Double");
	printf("length=%u\n",length);
	infile->readAttribute(j,buf);
	for(int k=0;k<length;k++)
	  printf("\tElement[%u]=%lf\n",k,buf[k]);
      } break;
      case IObase::Int32:{
	//int buf[5];
	puts("Integer");
	printf("length=%u\n",length);
      } break;
      default:
	puts("unknown type");
	break;
      }
    }
  }
  //delete reader;
  delete infile;
  puts("done");
  return 0;
}
