#ifndef __TIMER_HH_
#define __TIMER_HH_
#include <stdio.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/time.h>

class Timer {
  int running;
  double treal,tuser,tsystem;
  tms tm;
  timeval tv;
public:
  Timer() { reset(); }
  void reset(){
    treal=tuser=tsystem=0;
    running=0;
  }
  int start();
  int stop();
  void elapsedTimeSeconds(double &system,double &user,double &real);
  void elapsedTimeSeconds(float &system,float &user,float &real){
    double s,u,r;
    elapsedTimeSeconds(s,u,r);
    system=s; user=u; real=r;
  }
  void print(char *preface="",FILE *f=stdout);
};

class Counter {
	int count;
public:
	Counter():count(0){}
	void reset() {count = 0;}
	int incr(){ return count++;}
	int nCount() { return count;}
};

#endif
