//------------------------------------------------------------------------------
//
// Project:	Crackfree Isosurfaces for AMR data
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/*
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA and Germany"
 *
 */
 
#ifndef _GHW_AMRGRIDSTITCHER_HH_
#define _GHW_AMRGRIDSTITCHER_HH_

// Standard C/C++ Includes
#include <algorithm>
#include <iterator>
#include <list>

// Support Classes
#include <Vertex.hh>

// AMR Specific Includes
#include <AMRHierarchy.hh>
#include <AMRGridCell.hh>

class AMRGrid;

// AMRGridStitcher:
//    Connect Grids in AMR Hierarchy via stitch cells,
//    see WriteStitchCells for an example
//    AMRGridStitcher is a template of a class which is supposed to
//    handle the stitch cells and do something with them, e.g. write them
//    to a file (as StitchCellWriter in the WriteStitchCells example does)
//    This class must implement the following methods:
//      void handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const Vertex& p3, double v3);
//        --> Handle a tetrahedron cell with one Vertex that is not an original data value (computed as centroid of original data vertices)
//      void handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3);
//        --> Handle a tetrahedron cell with vertices described by GridCells, i.e. one only using original data points
//      void handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const Vertex& p4, double v4);
//        --> Handle a pyramid cell with one Vertex that is not an original data value (computed as centroid of original data vertices)
//      void handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4);
//        --> Handle a pyramid cell with vertices described by GridCells, i.e. one only using original data points
//      void handleTrianglePrismCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5);
//        --> Handle a triangle prism cell with vertices described by GridCells, i.e. one only using original data points
//      void handleHexahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5, const AMRGridCell& c6, const AMRGridCell& c7);
//        --> Handle a hexahedron cell with vertices described by GridCells, i.e. one only using original data points
//        Concerning the ordering scheme of vertices of the stitch-cells see VtxOrder.pdf or VtxOrder.png
 
template<class StitchCellHandler>
class AMRGridStitcher {
  public:
    // Generate a grid stitcher:
    // stitchCellHandler - object which uses the generated cells
    // amrHier - the AMR hierarchy
    // stitchGrid - Grid that should be stitched into the parent level
    // considerNextLevel - if true: do not generate those stitch cells that would be covered by cells/stitch cells of a next level,
    //    i.e., when cells connecting two grids are generated and this is set to true, all cells that are overlaped by grids of
    //    the next level are ignored. If false: any effects a next level may have are ignored
    AMRGridStitcher(StitchCellHandler& stitchCellHandler, AMRHierarchy& amrHier, AMRGrid *stitchGrid, bool considerNextLevel=false);
    ~AMRGridStitcher();
    // Start the stitching process
    void generateStitchCells(bool xyMin = true, bool xyMax = true, bool yzMin = true, bool yzMax = true, bool xzMin = true, bool xzMax = true);

  private: // Helper functions and data types
    enum MinMaxType { min, max };

    void connectQuadToVertex(const AMRGridCell& qp0, const AMRGridCell& qp1, const AMRGridCell& qp2, const AMRGridCell& qp3, const AMRGridCell& vp, AxisType axis0, AxisType axis1, AxisType axis2, MinMaxType faceType);
    void connectQuadToEdge(const AMRGridCell& qp0, const AMRGridCell& qp1, const AMRGridCell& qp2, const AMRGridCell& qp3, const AMRGridCell& ep0, const AMRGridCell& ep1, AxisType axis01, AxisType axis03, AxisType axisPerp0123, MinMaxType qp0Axis01Type, MinMaxType qp0Axis03Type, MinMaxType quadFaceType);
    void connectQuadToQuad(const AMRGridCell& qp0, const AMRGridCell& qp1, const AMRGridCell& qp2, const AMRGridCell& qp3, const AMRGridCell& oqp0, const AMRGridCell& oqp1, const AMRGridCell& oqp2, const AMRGridCell& oqp3, AxisType axis01, AxisType axis03, AxisType axisPerp0123, MinMaxType qp0Axis01Type, MinMaxType qp0Axis03Type, MinMaxType quadFaceType);

    void connectEdgeSegmentToEdges(const AMRGridCell& ep0, const AMRGridCell& ep1, const AMRGridCell& cp0, const AMRGridCell& cp1, const AMRGridCell& cp2, AxisType edgeAxis, AxisType edge01Axis, MinMaxType edgeAxis01Type, AxisType edge02Axis, MinMaxType edgeAxis02Type);
    void connectEdgeSegmentToQuads(const AMRGridCell& ep0, const AMRGridCell& ep1, const AMRGridCell& cp0, const AMRGridCell& cp1, const AMRGridCell& cp2, const AMRGridCell& cp3, const AMRGridCell& cp4, const AMRGridCell& cp5, AxisType edgeAxis, AxisType addAxis0132, MinMaxType edgeAxis0132Type, AxisType addAxis0451, MinMaxType edgeAxis0451Type);

    void connectVertexToQuad(const AMRGridCell& vtx, const AMRGridCell& p0, const AMRGridCell& p1, const AMRGridCell& p2, const AMRGridCell& p3);

    void generateCellsForFace(AxisType axis1, AxisType axis2, AxisType axis3, MinMaxType faceType);
    void generateCellsForEdge(AxisType edgeAxis, AxisType nonEdgeAxis1, MinMaxType nonEdgeAxis1Type, AxisType nonEdgeAxis2, MinMaxType nonEdgeAxis2Type);
    void generateCellsForVertex(AxisType axis0, AxisType axis1, AxisType axis2, MinMaxType axis0VertexType, MinMaxType axis1VertexType, MinMaxType axis2VertexType);

  private: // Data members
    StitchCellHandler &mStitchCellHandler;
    AMRHierarchy &mAMRDataSet;
    AMRGrid *mStitchGrid;
    bool mConsiderNextLevel;
    AMRLevel *mParentLevel;
    int mOriginInParentLevel[3];
    int mMaxExtInParentLevel[3];
};

#ifdef __GNUC__
#include "AMRGridStitcher.cc"
#endif

#endif
