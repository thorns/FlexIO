//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/* WARNING: This software is EXPERIMENTAL. The authors give no guaranty
 *   about stability or correctness of the provided source!
 *
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA [and Germany :-)]"
 *
 */
 
#include "AMRHierarchy.hh"

// Standard C/C++-Includes
#include <iostream>
#include <math.h>

// AMR related includes
#include <IEEEIO.hh>
#include <AmrGridReader.hh>
#include <AMRGrid.hh>
#include <AMRLevel.hh>

AMRHierarchy::AMRHierarchy() : mAmrFile(NULL), mAmrGridReader(NULL), mNoLevels(0), mNoGrids(0), mAMRLevel(NULL), mIndexToGridPtrMap(NULL), mValueRange(0.0,0.0) {}

AMRHierarchy::~AMRHierarchy()
{
  clear();
}


bool AMRHierarchy::readIEEEFile(const char *filename)
{
  // Create the object for file-access. This allows AmrGridReader to read different
  // file formats.
  IEEEIO *theAmrFile = new IEEEIO(filename, IObase::Read);
  if (!(theAmrFile!=NULL && theAmrFile->isValid())) {
    std::cerr << "Error in AMRHierarchy::readIEEEFile(char *): Couldn't open AMR-file" << filename << " for reading" << std::endl;
    return false;
  }
  return readFromFile(theAmrFile);
}

bool AMRHierarchy::readFromFile(IObase *theAmrFile)
{
  // Clear old grid data
  clear();

  mAmrFile = theAmrFile;

  // Create the object for reading the AMR Hierarchy
  mAmrGridReader = new AmrGridReader(*mAmrFile);

  GridId currentGridIdx=0;	// This is the index of the current grid within the AMR file
  std::list<AMRGrid *>allGrids;		// List of all AMR Grids
  
  AMRGrid *newGrid = new AMRGrid(currentGridIdx, this, mAmrGridReader);
  while (mAmrGridReader->getGridInfo(*newGrid, currentGridIdx)) {
    newGrid->computeValueRange();
    std::cout << "Read grid " << currentGridIdx << std::endl; 
    // <TEMPORARY>
    // This is because the iorigin in the data set isn't always correct....
    newGrid->iorigin[0] = int(newGrid->origin[0]/(newGrid->delta[0])+0.5);
    newGrid->iorigin[1] = int(newGrid->origin[1]/(newGrid->delta[1])+0.5);
    newGrid->iorigin[2] = int(newGrid->origin[2]/(newGrid->delta[2])+0.5);
    // </TEMPORARY>
    if (newGrid->level+1>mNoLevels) mNoLevels=newGrid->level+1;
    allGrids.push_back(newGrid);
    ++currentGridIdx;
    newGrid = new AMRGrid(currentGridIdx, this, mAmrGridReader);
  }
  delete newGrid;	// Space allocated for a grid which couldn't be read

  // Create the list of levels
  AMRGrid **firstGridInLevel = new AMRGrid*[mNoLevels];
  for (int currLevel=0; currLevel<mNoLevels; ++currLevel) firstGridInLevel[currLevel]=NULL;
  std::list<AMRGrid*>::iterator gridIter;
  for (gridIter=allGrids.begin(); gridIter!=allGrids.end(); ++gridIter) {
    int level = (*gridIter)->level;
    (*gridIter)->mNextGridInLevel = firstGridInLevel[level];
    firstGridInLevel[level] = (*gridIter); 
  }

  // Create tree/graph structure
  for (int currLevel=0; currLevel<mNoLevels-1; ++currLevel) {
    for (AMRGrid *currParent=firstGridInLevel[currLevel];
	currParent!=NULL;
	currParent=currParent->mNextGridInLevel) {
      for (AMRGrid *currPotentialChild=firstGridInLevel[currLevel+1];
	  currPotentialChild!=NULL;
	  currPotentialChild=currPotentialChild->mNextGridInLevel) {
	if (currPotentialChild->overlaps(*currParent)) {
	  currParent->mRefiningGrids.push_back(currPotentialChild);
	  currPotentialChild->mRefinedGrids.push_back(currParent);
	} // if 
      } // for (currPotentialChild...)
    } // for (currParent...)
  } // for (currLevel...)

  // Create AMRLevel
  mAMRLevel = new AMRLevel*[mNoLevels];
  for (int currLevel=0; currLevel<mNoLevels; ++currLevel)
    mAMRLevel[currLevel] = new AMRLevel(firstGridInLevel[currLevel]);
  delete[] firstGridInLevel;

  // Copy list of pointers to all grids to gridIndex, for direct access to grids via index
  mNoGrids = allGrids.size();
  mIndexToGridPtrMap = new AMRGrid*[mNoGrids];
  for (gridIter = allGrids.begin(); gridIter != allGrids.end(); ++gridIter) {
    assert((*gridIter)->index() < mNoGrids);
    mIndexToGridPtrMap[(*gridIter)->index()] = *gridIter;
  }

  mValueRange = mIndexToGridPtrMap[0]->getValueRange();
  for (GridId currGridIdx = 1; currGridIdx < mNoGrids; ++currGridIdx) {
    mValueRange.combine(mIndexToGridPtrMap[currGridIdx]->getValueRange());
  }
  std::cout << "Value range " << mValueRange.min << " to " << mValueRange.max << std::endl;

  return true;
}

void AMRHierarchy::clear()
{
  delete mAmrFile;
  mAmrFile = NULL;
  delete mAmrGridReader;
  mAmrGridReader = NULL;

  for (int currLevel=0; currLevel < mNoLevels; ++currLevel)
    delete mAMRLevel[currLevel];
  delete[] mAMRLevel;
  mAMRLevel = NULL;
  mNoLevels = 0;
  for (int i=0; i<mNoGrids; ++i) delete mIndexToGridPtrMap[i];
  mNoGrids = 0;
  delete[] mIndexToGridPtrMap; 
  mIndexToGridPtrMap= NULL;

  mValueRange.setRange(0.0,0.0);
}
