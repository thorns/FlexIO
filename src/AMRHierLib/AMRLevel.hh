//------------------------------------------------------------------------------
//
// Project:     AMR Visualization
// Module:      $RCSfile$
// Language:    C++
// Date:        $Date$
// Author:      $Author$
// Version:     $Revision$
//
//------------------------------------------------------------------------------

/*
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA [and Germany :-)]"
 *
 */
 
#ifndef _GHW_AMRLEVEL_HH_
#define _GHW_AMRLEVEL_HH_

// Standard C/C++ Includes
#include <iostream>

// Own Misc. Helper Includes
#include <AxisType.hh>

// AMR Related Includes
#include <AMRHierarchy.hh>
#include <AMRGridCell.hh>

class AMRGrid;

/**
 * One level of an AMR hierarchy.
 * Holds one level of the AMR hierarchy and enables queries over an entire
 * level rather than single grids.
 *
 */
class AMRLevel {
  public:
    /**
     * Construct an AMR level
     * Construct an AMR level from a linked list of grids.
     * \param firstGridInLevel pointer to the first grid in this level.
     * Each grid contains has method getNextGridInLevel() that returns
     * the next grid in the level. The list must already be complete when
     * this constructor is called.
     */
    AMRLevel(AMRGrid *firstGridInLevel);
    /**
     * Get GridPoint for given position
     * Get structure containing pointer to grid containing the given location
     * and index of that location within that grid.
     * \param i Index along x-Axis within <em>level</em>
     * \param j Index along y-Axis within <em>level</em>
     * \param k Index along z-Axis within <em>level</em>
     * \return Pointer to \see{GridPoint} structure or NULL, if no grid contains
     * the given location
     */
    AMRGridCell getGridCell(int i, int j, int k) const;
    /**
     * Get GridPoint for given position
     * Get structure containing pointer to grid containing the given location
     * and index of that location within that grid.
     * \param idx Array containing indices within <em>level</em>
     * \return Pointer to \see{GridPoint} structure or NULL, if no grid contains
     * the given location
     */
    AMRGridCell getGridCell(int idx[3]) const;
    /**
     * Get index of grid containing absolutes position.
     * Gets the index of a grid that contains the position given in level
     * coordsinates.
     * \param i Index along x-Axis within <em>level</em>
     * \param j Index along y-Axis within <em>level</em>
     * \param k Index along z-Axis within <em>level</em>
     * \return GridId of grid containing that location in the level, or AMRGrid::NoGrid (-1)
     * if no grid contains that location.
     */
    AMRHierarchy::GridId getGridId(int i, int j, int k) const;
    /**
     * Get index of grid containing absolutes position.
     * Gets the index of a grid that contains the position given in level
     * coordsinates.
     * \param idx Array containing indicrs within <em>level>
     * \return GridId of grid containing that location in the level, or AMRGrid::NoGrid (-1)
     * if no grid contains that location.
     */
    AMRHierarchy::GridId getGridId(int idx[3]) const;

    /**
     * Get pointer to first grid in this level
     * Get pointer to first grid in this level.
     * \return Pointer to first grid in that level.
     */
    AMRGrid* getFirstGrid() const;

    /**
     * Get spatial refinement along a given axis.
     */
    int getSpatialRefinement(AxisType axis) const;

    /**
     * Exception indicating no grid
     */
    class NoGrid {
      public:
	NoGrid(int i, int j, int k);
      private:
	friend std::ostream& operator<<(std::ostream& os, const AMRLevel::NoGrid &ng);
	int mPos[3];
    };

  private:
    // Pointer to the first grid of this level. All grids are
    // stored in a linked list. The pointer to the next grid
    // in this level is AMRGrid::mNextGridInLevel and can be
    // accessed by AMRGrid::getNextGridInLevel.
    AMRGrid* mFirstGridInLevel;
    // This information is also found in the individual grids.
    // It is also stored here for convinience.
    int mLevelNo;
    int mSpatialRefinement[3];
    int mPlacementRefinement[3];
};

#include <AMRLevel.icc>

#endif
