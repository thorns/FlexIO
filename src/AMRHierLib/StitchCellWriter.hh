//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/*
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA and Germany"
 *
 */
 
#ifndef _GHW_STITCHCELLWRITER_HH_
#define _GHW_STITCHCELLWRITER_HH_

// Standard C/C++ Includes
#include <fstream>

// Own Helper Includes
#include <BinStream.hh>

// AMR Related Includes
#include <AMRGrid.hh>
#include <AMRGridCell.hh>

// A StitchCellCollector that writes all stitch cells to files
class StitchCellWriter {
  public:
    // Construction/Destruction
    StitchCellWriter(const AMRHierarchy& amrHier, const char* baseFilename, int maxLevel);
    ~StitchCellWriter();

    // Cell Types
    void handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const Vertex& p3, double v3);
    void handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3);
    void handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const Vertex& p4, double v4);
    void handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4);
    void handleTrianglePrismCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5);
    void handleHexahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5, const AMRGridCell& c6, const AMRGridCell& c7);
  private:
    // Private Types
    typedef float OutputFPType;

    // Private Memeber Functions
    int pointIdxForGridCell(const AMRGridCell& c);
    int addPoint(const Vertex& p, double v);

    // Private Data Members
    int mMaxLevel;
    int *mGridStartPointIdx;
    std::ofstream mPositionStream;
    std::ofstream mValueStream;
    int mNumPoints;
    std::ofstream mTetStream;
    int mNumTets;
    std::ofstream mPyramidStream;
    int mNumPyramids;
    std::ofstream mTrianglePrismStream;
    int mNumTrianglePrisms;
    std::ofstream mHexahedronStream;
    int mNumHexahedra;
};

#include "StitchCellWriter.icc"

#endif
