//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/* WARNING: This software is EXPERIMENTAL. The authors give no guaranty
 *   about stability or correctness of the provided source!
 *
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA [and Germany :-)]"
 *
 */
 
#include <AMRLevel.hh>

// Standard C/C++ Includes
#include <assert.h>
#include <iostream>

// Own Misc. Helper Includes
#include <assume.hh>

// AMR Related Includes
#include <AMRGrid.hh>
#include <AMRGridCell.hh>

AMRLevel::AMRLevel(AMRGrid *firstGridInLevel) : mFirstGridInLevel(firstGridInLevel), mLevelNo(mFirstGridInLevel->level)
{
  // All grids in any given level must have the same spatial- and placement-refinement
  // Thus, take these values from the first grid.
  mSpatialRefinement[0] = mFirstGridInLevel->spatialrefinement[0];
  mSpatialRefinement[1] = mFirstGridInLevel->spatialrefinement[1];
  mSpatialRefinement[2] = mFirstGridInLevel->spatialrefinement[2];
  mPlacementRefinement[0] = mFirstGridInLevel->gridplacementrefinement[0];
  mPlacementRefinement[1] = mFirstGridInLevel->gridplacementrefinement[1];
  mPlacementRefinement[2] = mFirstGridInLevel->gridplacementrefinement[2];

#ifndef NDEBUG
  // Perform a sanity check, whether above condition (same refinement ratio
  // for all grids in a level) is true. 
  for (AMRGrid *currGridPtr=mFirstGridInLevel; currGridPtr!=0; currGridPtr=currGridPtr->getNextGridInLevel()) {
    assert(mSpatialRefinement[0] == currGridPtr->spatialrefinement[0]);
    assert(mSpatialRefinement[1] == currGridPtr->spatialrefinement[1]);
    assert(mSpatialRefinement[2] == currGridPtr->spatialrefinement[2]);
    assert(mPlacementRefinement[0] == currGridPtr->gridplacementrefinement[0]);
    assert(mPlacementRefinement[1] == currGridPtr->gridplacementrefinement[1]);
    assert(mPlacementRefinement[2] == currGridPtr->gridplacementrefinement[2]);
  }
#endif

  // Check if the assumpution spatialrefinement == gridplacementrefinement holds. We rely
  // on this throughout further computations.
  assume(mSpatialRefinement[0] == mPlacementRefinement[0]);
  assume(mSpatialRefinement[1] == mPlacementRefinement[1]);
  assume(mSpatialRefinement[2] == mPlacementRefinement[2]);

  // Build a data structure enabling efficient access to a grid containing a given location
  // TODO: Finalize planing of acceleration structure and implement
}

AMRGridCell AMRLevel::getGridCell(int i, int j, int k) const
{
  for (AMRGrid *currGridPtr=mFirstGridInLevel; currGridPtr!=0; currGridPtr=currGridPtr->getNextGridInLevel()) {
    int idx[3];
    idx[0] = i - currGridPtr->iorigin[0];
    idx[1] = j - currGridPtr->iorigin[1];
    idx[2] = k - currGridPtr->iorigin[2];
    if (0 <= idx[0] && idx[0] < currGridPtr->dims[0] &&
	0 <= idx[1] && idx[1] < currGridPtr->dims[1] &&
	0 <= idx[2] && idx[2] < currGridPtr->dims[2]) {
      return AMRGridCell(currGridPtr, idx, AMRGrid::Unknown); // Lazy evaluation
    }
  }
  throw NoGrid(i, j, k);
}

AMRHierarchy::GridId AMRLevel::getGridId(int i, int j, int k) const
{
  // TODO: Add spatial search structure and make grid lookup more efficient enabling the use
  // for really large datasets.
  for (AMRGrid *currGridPtr=mFirstGridInLevel; currGridPtr!=0; currGridPtr=currGridPtr->getNextGridInLevel()) {
    int idx[3];
    idx[0] = i - currGridPtr->iorigin[0];
    idx[1] = j - currGridPtr->iorigin[1];
    idx[2] = k - currGridPtr->iorigin[2];
    if (0 <= idx[0] && idx[0] < currGridPtr->dims[0] &&
	0 <= idx[1] && idx[1] < currGridPtr->dims[1] &&
	0 <= idx[2] && idx[2] < currGridPtr->dims[2]) {
      return currGridPtr->index();
    }
  }
  return AMRGrid::NoGrid;
}

AMRLevel::NoGrid::NoGrid(int i, int j, int k)
{
  mPos[0] = i;
  mPos[1] = j;
  mPos[2] = k;
}

std::ostream& operator<<(std::ostream& os, const AMRLevel::NoGrid &ng)
{
  os << "No grid present at (" << ng.mPos[0] << ", " << ng.mPos[1] << ", " << ng.mPos[2] << ")";
  return os;
}
