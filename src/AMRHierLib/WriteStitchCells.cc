//------------------------------------------------------------------------------
//
// Project:	Tools for AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

// Standard C/C++ Includes
#include <algorithm>
#include <iostream>

// AMR Related Includes
#include <AMRHierarchy.hh>
#include <AMRLevel.hh>
#include <AMRGridStitcher.hh>

// Project specific Includes
#include "StitchCellWriter.hh"

int main(int argc, char* argv[])
{
  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <Input File> <Output Basename> <NumUsedLevels>" << std::endl;
    return 42;
  }
  AMRHierarchy theAMRHier;
  theAMRHier.readIEEEFile(argv[1]);
  int maxLevel = std::min(atoi(argv[3]) - 1, theAMRHier.numLevels() - 1);;
  StitchCellWriter writer(theAMRHier, argv[2], maxLevel);

  if (maxLevel > 0) {
    for (int currLevelNo = 1; currLevelNo < maxLevel; ++currLevelNo)
    {
      std::cout << "Start level " << currLevelNo << std::endl;
      AMRLevel *currLevel = theAMRHier.getLevel(currLevelNo);
      for (AMRGrid *currGridPtr = currLevel->getFirstGrid(); currGridPtr != 0; currGridPtr = currGridPtr->getNextGridInLevel()) {
	std::cout << "Stitching grid #" << currGridPtr->index() << std::endl;
	AMRGridStitcher<StitchCellWriter> gridStitcher(writer, theAMRHier, currGridPtr, true);
	gridStitcher.generateStitchCells();
      }
    }
    AMRLevel *lastLevel = theAMRHier.getLevel(maxLevel);
    for (AMRGrid *currGridPtr = lastLevel->getFirstGrid(); currGridPtr != 0; currGridPtr = currGridPtr->getNextGridInLevel()) {
      std::cout << "Stitching grid #" << currGridPtr->index() << std::endl;
      AMRGridStitcher<StitchCellWriter> gridStitcher(writer, theAMRHier, currGridPtr, false);
      gridStitcher.generateStitchCells();
    }
  }
}
