//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

#include <AMRGridCell.hh>

AMRGridCell::AMRGridCell(AMRGrid *g, int i, int j, int k, int id) : mGrid(g), mRefiningGridId(id)
{
  mIdx[0] = i;
  mIdx[1] = j;
  mIdx[2] = k;
  assert(0<=i && i<mGrid->dims[0]);
  assert(0<=j && j<mGrid->dims[1]);
  assert(0<=k && k<mGrid->dims[2]);
}

AMRGridCell::AMRGridCell(AMRGrid *g, int i[3], int id) : mGrid(g), mRefiningGridId(id)
{
  mIdx[0] = i[0];
  mIdx[1] = i[1];
  mIdx[2] = i[2];
  assert(0<=i[0] && i[0]<mGrid->dims[0]);
  assert(0<=i[1] && i[1]<mGrid->dims[1]);
  assert(0<=i[2] && i[2]<mGrid->dims[2]);
}
