//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

#include <IO.hh>
#include <IEEEIO.hh>
#include <fstream>
#include <zlib.h>
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc!=3) {
    std::cerr << "Usage: " << argv[0] << " <volFilename> <ieeeioFilename>" << std::endl;
    return 42;
  }

  gzFile in;
  if ((in = gzopen(argv[1], "rb"))) {
    int dims[3];
    double delta[3];
    bool success = true;
    success = success &&  gzread(in, &dims[0], sizeof(int)) == sizeof(int);
    success = success &&  gzread(in, &dims[1], sizeof(int)) == sizeof(int);
    success = success &&  gzread(in, &dims[2], sizeof(int)) == sizeof(int);
    success = success &&  gzread(in, &delta[0], sizeof(double)) == sizeof(double);
    success = success &&  gzread(in, &delta[1], sizeof(double)) == sizeof(double);
    success = success &&  gzread(in, &delta[2], sizeof(double)) == sizeof(double);
    if (success) {
      unsigned char* bytedata = new unsigned char[dims[0]*dims[1]*dims[2]];
      if (gzread(in, bytedata, dims[0]*dims[1]*dims[2]*sizeof(unsigned char)) == dims[0]*dims[1]*dims[2]*sizeof(unsigned char)) {
	float *data = new float[dims[0]*dims[1]*dims[2]];
	for (int i=0; i<dims[0]*dims[1]*dims[2]; ++i) {
	  data[i] = double(bytedata[i]);
	}
	IObase *writer = new IEEEIO(argv[2], IObase::Create);
	int rank = 3;
	writer->write(IObase::Float32, rank, dims, data);
	double origin[3] = {0.0, 0.0, 0.0};
	writer->writeAttribute("origin", IObase::Float64,3,origin);
	writer->writeAttribute("min_ext", IObase::Float64,3,origin);
	double maxExt[3] = {(dims[0]+1)*delta[0],(dims[1]+1)*delta[1],(dims[2]+1)*delta[2]};
	writer->writeAttribute("max_ext", IObase::Float64,3,maxExt);
	int level = 0;
	writer->writeAttribute("level", IObase::Int32, 1, &level);
	writer->writeAttribute("delta", IObase::Float64, 3, delta);
	int refine[3] = { 1, 1, 1 };
	writer->writeAttribute("spatial_refinement", IObase::Int32, 3, refine);
	writer->writeAttribute("grid_placement_refinement", IObase::Int32, 3, refine);
	int iorigin[3] = { 0, 0, 0 };
	writer->writeAttribute("iorigin", IObase::Int32,3,iorigin);
	delete data;
	delete writer;
      }
      delete bytedata;
    }
    gzclose(in);
  }
}
