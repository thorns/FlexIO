//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/*
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA and Germany"
 *
 */
 
#ifndef _GHW_AMRHIERARCHY_H_
#define _GHW_AMRHIERARCHY_H_

// Misc. includes
#include <Range.hh>

// AMR related includes
#include <IO.hh>

class AMRLevel;
class AMRGrid;
class AmrGridReader;

/*
 * Access to the hierarchy of an AMR data set:
 * This class is a wrapper to the AMR structures 
 */
class AMRHierarchy {
  public:
    typedef int GridId;
    // Read an AMR File in John's IEEE format from disk
    bool readIEEEFile(const char *filename);
    // Read arbitrary AMR file formats, if a reader is given. This is currently
    // only used on readIEEEFile. See that function for details.
    bool readFromFile(IObase *theAmrFile);
    // Erase hierarchy
    void clear();
    // How many levels are in the hierarchy?
    int numLevels() const;
    // How many grids?
    int numGrids() const;
    // Each grid in the hierachy has an index 0 <= gridIndex < numGrids(). This
    // function allows to get a grid without any regard to its position in the hierarchy.
    AMRGrid* getGridByIndex(GridId gridIndex) const;
    // Get an AMRLevel object (see corresponding header). This gives access to a list of grids
    // of that level. AMRLevel also provides some helper functions, e.g. translating a global
    // level index to a pointer to a grid and an index within that grid.
    AMRLevel* getLevel(int levelNo) const;
    // Gets the value range in the entire hierarchy
    const Range<float> &getValueRange() const;
    // Create an empty hierarchy object
    AMRHierarchy();
    // Clean up.
    ~AMRHierarchy();
  private:
    friend class AMRGrid;
    // Accessing the data on disk:
    IObase *mAmrFile;
    AmrGridReader *mAmrGridReader;

    // Accessing the data in memory: 
    int mNoLevels;				// Number of levels in hierarchy
    int mNoGrids;				// Number of grids in hierarchy (all levels!)
    AMRLevel **mAMRLevel;			// Array 0..mNumLevels-1 of pointers to AMRLevel objects
    AMRGrid **mIndexToGridPtrMap;		// Array that maps each grid index to a pointer to the corresponding grid 

    // Additional Info about grid-data:
    Range<float> mValueRange;				// Range of values in all grids

    // As seen in Scott Meyer's Effective C++: If you don't want to specify copy constructor
    // and assignment operator, make them inaccessible to prevent them from being called accidently.
    // NOTE: Even though declared here, they are not defined (causes link error when accidently needed
    // within class).
    AMRHierarchy(const AMRHierarchy&);			// Prevent copying
    AMRHierarchy& operator=(const AMRHierarchy&);	// Prevent assignment
};

#include "AMRHierarchy.icc"

#endif
