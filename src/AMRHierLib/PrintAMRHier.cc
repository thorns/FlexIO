#include <iostream>
#include "AMRHierarchy.hh"
#include "AMRLevel.hh"
#include "AMRGrid.hh"

int main(int argc, char *argv[])
{
  AMRHierarchy theAMRHier;

  if (argc!=2) {
    std::cerr << "Usage: " << argv[0] << " <amrHierFile>" << std::endl;
    return 27;
  }

  if (!theAMRHier.readIEEEFile(argv[1])) {
    std::cerr << "Could not read AMR file " << argv[1] << std::endl;
  }

  for (int levelNo=0; levelNo<theAMRHier.numLevels(); ++levelNo) {
    std::cout << "********** Level " << levelNo << " of " << theAMRHier.numLevels() << "**********" << std::endl;
    for (AMRGrid *currGridPtr = theAMRHier.getLevel(levelNo)->getFirstGrid(); currGridPtr != 0; currGridPtr = currGridPtr->getNextGridInLevel())
      currGridPtr->printInfo(std::cout);
  }
}
