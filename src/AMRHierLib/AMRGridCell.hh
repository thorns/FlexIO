//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

/*
 * Copyright (C) 2001,  The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the Visualization
 *      Group at Lawrence Berkeley National Laboratory.
 * 4. Neither the name of the University of California, Berkeley nor of the 
 *    Lawrence Berkeley National Laboratory may be used to endorse or 
 *    promote products derived from this software without specific prior 
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * This work is supported by the U. S. Department of Energy under contract 
 * number DE-AC03-76SF00098 between the U. S. Department of Energy and the 
 * University of California.
 *
 *	Author: Gunther H Weber
 *		Lawrence Berkeley National Laboratory
 *              Berkeley, California 
 *
 *  "this software is 100% hand-crafted by a human being in the USA and Germany"
 *
 */
 
#ifndef _GHW_GRIDCELL_
#define _GHW_GRIDCELL_

// Own Misc. Helper Includes
#include <AxisType.hh>
#include <Vertex.hh>

// AMR Related Includes
#include <AMRGrid.hh>

/**
 * Describe a grid cell
 * Describe a grid cell via pointer to grid containing it and its
 * index within that grid.
 */
class AMRGridCell {
  public:
    /**
     * Construct a GridCell
     * Consturct a new GridCell
     * \param g pointer to grid containing the cell
     * \param i index of cell along x-Axis
     * \param j index of cell along y-Axis
     * \param k index of cell along z-Axis
     * \param id ID of grid refining this cell, if known
     */
    AMRGridCell(AMRGrid *g, int i, int j, int k, AMRHierarchy::GridId id=AMRGrid::Unknown);
    /**
     * Construct a GridCell
     * Consturct a new GridCell
     * \param g pointer to grid containing the cell
     * \param i index of cell
     * \param id ID of grid refining this cell, if known
     */
    AMRGridCell(AMRGrid *g, int i[3], AMRHierarchy::GridId id=AMRGrid::Unknown);
    /**
     * Get absolute position of center of the grid cell 
     * Calaculate the absoulte position of the center of the grid cell, i.e. the
     * location associated with the grid cell sample value.
     */
    Vertex pos() const;
    /** 
     * Get value associated with grid point
     * Get the scalar value that is stored in the grid for the given grid point.
     */
    double val() const;
    /**
     * Get index of grid containing the grid-cell
     * Get the index/ID of the grid that contains this grid cell
     */
    int containingId() const;
    /**
     * Get index of grid refining the grid-cell
     * Get the index/ID of the grid that refines this grid cell
     */
    int refiningId() const;
    /**
     * Get a pointer to the AMRGrid containing the cell
     */
    AMRGrid *grid() const;
    /**
     * Get index in 'grid coordinates'
     */
    int idx(AxisType axis) const;
  private:
    AMRGrid *mGrid;			// Pointer to the grid containing the cell
    int mIdx[3];			// Index of the cell
    mutable int mRefiningGridId;	// Index of the grid that refines this grid cell
};

#include <AMRGridCell.icc>

#endif
