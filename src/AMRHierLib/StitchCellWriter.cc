//------------------------------------------------------------------------------
//
// Project:	AMR Visualization
// Module:	$RCSfile$
// Language:	C++
// Date:	$Date$
// Author:	$Author$
// Version:	$Revision$
//
//------------------------------------------------------------------------------

#include "StitchCellWriter.hh"

// Standard C/C++ Includes
#include <string>

// Own Misc. Helper Includes
#include <BinStream.hh>

// AMR Includes
#include <AMRHierarchy.hh>
#include <AMRGrid.hh>
#include <AMRGridCell.hh>

StitchCellWriter::StitchCellWriter(const AMRHierarchy& amrHier, const char* baseFilename, int maxLevel)
  : mMaxLevel(maxLevel),
    mGridStartPointIdx(new int[amrHier.numGrids()+1]),
    mPositionStream((std::string(baseFilename) + std::string(".pts")).c_str()),
    mValueStream((std::string(baseFilename) + std::string(".vals")).c_str()),
    mNumPoints(0),
    mTetStream((std::string(baseFilename) + std::string(".tets")).c_str()),
    mNumTets(0),
    mPyramidStream((std::string(baseFilename) + std::string(".pyramids")).c_str()),
    mNumPyramids(0),
    mTrianglePrismStream((std::string(baseFilename) + std::string(".triangleprisms")).c_str()),
    mNumTrianglePrisms(0),
    mHexahedronStream((std::string(baseFilename) + std::string(".hexahedra")).c_str()),
    mNumHexahedra(0)
{
  // Write 0 as number of points, tets, ...
  // Destructor seeks back and writes actual numbers when they are known (no more points,
  // tets, ...) added
  BinStream::binaryOut(mPositionStream, mNumPoints);
  BinStream::binaryOut(mTetStream, mNumTets);
  BinStream::binaryOut(mPyramidStream, mNumPyramids);
  BinStream::binaryOut(mTrianglePrismStream, mNumTrianglePrisms);
  BinStream::binaryOut(mHexahedronStream, mNumHexahedra);

  mGridStartPointIdx[0] = 0;
  for (int currGridIdx=0; currGridIdx < amrHier.numGrids(); ++currGridIdx) {
    AMRGrid *currGrid = amrHier.getGridByIndex(currGridIdx);

    if (currGrid->level > mMaxLevel) {
      // Skip grid
      std::cout << "Skipping grid " << currGrid->index() << ", level " << currGrid->level << " > " << mMaxLevel << std::endl;
      mGridStartPointIdx[currGridIdx+1] = mGridStartPointIdx[currGridIdx];
    }
    else {
      currGrid->loadCells();
      // Write data points
      Vertex pos;
      int i, j, k;
      OutputFPType out;
      // (i, j, k) -> mGridStartPointIdx[gridIdx] + (k * dims[1] + j) * dims[0] + i
      for (k=0, pos.z = currGrid->origin[2]+0.5*currGrid->delta[2]; k < currGrid->dims[2]; ++k, pos.z += currGrid->delta[2])
	for (j=0, pos.y = currGrid->origin[1]+0.5*currGrid->delta[1]; j < currGrid->dims[1]; ++j, pos.y += currGrid->delta[1])
	  for (i=0, pos.x = currGrid->origin[0]+0.5*currGrid->delta[0]; i < currGrid->dims[0]; ++i, pos.x += currGrid->delta[0]) {
	    out = pos.x; BinStream::binaryOut(mPositionStream, out);
	    out = pos.y; BinStream::binaryOut(mPositionStream, out);
	    out = pos.z; BinStream::binaryOut(mPositionStream, out);
	    out = currGrid->getValue(i, j, k); BinStream::binaryOut(mValueStream, out);
	  }
      int numPointsThisGrid = currGrid->dims[0]*currGrid->dims[1]*currGrid->dims[2];
      mGridStartPointIdx[currGridIdx+1] = mGridStartPointIdx[currGridIdx] + numPointsThisGrid;
      mNumPoints += numPointsThisGrid;

      AMRGrid::IntersectionMap *intersectionMap = currGrid->computeIntersectionMap(AMRGrid::IntersectionMap::RefinedCells);
      // Write dual grid
      for (i=0; i<currGrid->dims[0]-1; ++i)
	for (j=0; j<currGrid->dims[1]-1; ++j)
	  for (k=0; k<currGrid->dims[2]-1; ++k)
	    if (((*intersectionMap)(i  , j  , k  ) == -1) && ((*intersectionMap)(i  , j  , k+1) == -1) &&
		((*intersectionMap)(i  , j+1, k  ) == -1) && ((*intersectionMap)(i  , j+1, k+1) == -1) && 
		((*intersectionMap)(i+1, j  , k  ) == -1) && ((*intersectionMap)(i+1, j  , k+1) == -1) &&
		((*intersectionMap)(i+1, j+1, k  ) == -1) && ((*intersectionMap)(i+1, j+1, k+1) == -1)
		|| currGrid->level == maxLevel)
	      handleHexahedronCell(
		  AMRGridCell(currGrid, i  , j  , k  ),
		  AMRGridCell(currGrid, i+1, j  , k  ),
		  AMRGridCell(currGrid, i+1, j+1, k  ),
		  AMRGridCell(currGrid, i  , j+1, k  ),
		  AMRGridCell(currGrid, i  , j  , k+1),
		  AMRGridCell(currGrid, i+1, j  , k+1),
		  AMRGridCell(currGrid, i+1, j+1, k+1),
		  AMRGridCell(currGrid, i  , j+1, k+1)
				  );
      delete intersectionMap;
    }
  }
}

StitchCellWriter::~StitchCellWriter()
{
  // Write actual number of points, tets, ...
  mPositionStream.seekp(0);
  BinStream::binaryOut(mPositionStream, mNumPoints);
  std::cout << "Wrote " << mNumPoints << " positions and values." << std::endl;
  mTetStream.seekp(0);
  BinStream::binaryOut(mTetStream, mNumTets);
  std::cout << "Wrote " << mNumTets << " tetrahedra." << std::endl;
  mPyramidStream.seekp(0);
  BinStream::binaryOut(mPyramidStream, mNumPyramids);
  std::cout << "Wrote " << mNumPyramids << " pyramids." << std::endl;
  mTrianglePrismStream.seekp(0);
  BinStream::binaryOut(mTrianglePrismStream, mNumTrianglePrisms);
  std::cout << "Wrote " << mNumTrianglePrisms << " triangle prisms." << std::endl;
  mHexahedronStream.seekp(0);
  BinStream::binaryOut(mHexahedronStream, mNumHexahedra);
  std::cout << "Wrote " << mNumHexahedra << " hexahedra." << std::endl;
}

void StitchCellWriter::handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const Vertex& p3, double v3)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c0));
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mTetStream, addPoint(p3, v3));
  ++mNumTets;
}

void StitchCellWriter::handleTetrahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c0));
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mTetStream, pointIdxForGridCell(c3));
  ++mNumTets;
}

void StitchCellWriter::handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const Vertex& p4, double v4)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c0));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c3));
  BinStream::binaryOut(mPyramidStream, addPoint(p4, v4));
  ++mNumPyramids;
}

void StitchCellWriter::handlePyramidCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c0));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c3));
  BinStream::binaryOut(mPyramidStream, pointIdxForGridCell(c4));
  ++mNumPyramids;
}

void StitchCellWriter::handleTrianglePrismCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c3));
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c0));
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c4));
  BinStream::binaryOut(mTrianglePrismStream, pointIdxForGridCell(c5));
  ++mNumTrianglePrisms;
}

void StitchCellWriter::handleHexahedronCell(const AMRGridCell& c0, const AMRGridCell& c1, const AMRGridCell& c2, const AMRGridCell& c3, const AMRGridCell& c4, const AMRGridCell& c5, const AMRGridCell& c6, const AMRGridCell& c7)
{
  // Adapt vertex ordering to Nelson Max's ordering scheme
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c7));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c6));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c2));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c1));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c4));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c5));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c3));
  BinStream::binaryOut(mHexahedronStream, pointIdxForGridCell(c0));
  ++mNumHexahedra;
}
