#include <iostream.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "SockIOwriter.hh"

SockIOwriter::SockIOwriter(CONST char *hostname,int port):IObase(hostname,IObase::Write),
  clientsock(0),ndatasets(0),datasetnumber(-1){
    //RawTCPclient *clientsock = new RawTCPclient(hostname,port);
    clientsock = new RawTCPclient(hostname,port); // One Megabyte window
    fprintf(stderr,"ClientSock=%lu\n",(long)clientsock);
    // this will block until it connects
}

SockIOwriter::SockIOwriter(CONST char *hostname,int port,int windowsize):IObase(hostname,IObase::Write),
    clientsock(0),ndatasets(0),datasetnumber(-1){ 
      clientsock = new FastTCPclient(hostname,port,windowsize); // open bigwindow socket
      fprintf(stderr,"Open ClientSock=%lu with %u windowsize\n",(long)clientsock,windowsize);
}

SockIOwriter::~SockIOwriter(){
  delete clientsock;
}

int SockIOwriter::isValid(){
  if(clientsock && clientsock->isAlive())
    return 1;
  else 
    return 0;
}

// could use overloading to differentiate type here... (but I'm going simple)
int SockIOwriter::write(IObase::DataType typeID,int rank,CONST int *dims,const void *data){
  register int i;
  Int sdims[5];
  for (i = 0; i < rank ; i++)
    sdims[i] = dims[i];
  RecordHdr rec;
  DataRecordHdr hdr;
  if(!isValid()){
    puts("no valid connection");
    return 0;
  }
  for(i=0,hdr.datasize=1;i<rank;i++) hdr.datasize*=dims[i];
  hdr.datasize*=sizeOf(typeID);
  hdr.numbertype=typeID;
  hdr.rank=rank;
  rec.recordtype = DataRecord;
  rec.recordsize = hdr.datasize + 
    sizeof(DataRecordHdr) + 
    sizeof(int) * rank;
  rec.sequenceID = datasetnumber = ndatasets++;
  current_rec=rec;
  fprintf(stderr,"Current clientsock=%lu\n",(unsigned long)clientsock);
  fprintf(stderr,"Portnumber=%u\n",clientsock->getPortNum());
  fprintf(stderr,"Socket validity=%u\n",clientsock->isAlive());
  int sz = RecordHdr::write(rec, clientsock);
  sz = DataRecordHdr::write(hdr, clientsock);
  
  //clientsock->write((char*)(&rec),sizeof(rec));
  //clientsock->write((char*)(&hdr),sizeof(hdr));
  clientsock->write((char*)sdims,rank*sizeof(Int));
  return clientsock->write((char*)data,hdr.datasize);
}

int SockIOwriter::writeAnnotation(CONST char *annotation){
  RecordHdr rec;
  int stringlen=strlen(annotation)+1;
  rec.recordtype=AnnotationRecord;
  rec.recordsize=stringlen;
  if(datasetnumber>=0) rec.sequenceID=datasetnumber;
  else rec.sequenceID=current_rec.sequenceID;
  clientsock->write((char*)(&rec),sizeof(RecordHdr));
  return clientsock->write((char*)annotation,stringlen);
}

// Write a NetCDF attribute across the socket connection
int SockIOwriter::writeAttribute(CONST char *name,IObase::DataType typeID,Long length,const void *data){
  int i;
  AttributeRecordHdr attrib;
  RecordHdr rec;
  int stringlen=strlen(name)+1;

  attrib.datasize=length*sizeOf(typeID);
  attrib.namesize=stringlen;
  attrib.numbertype=typeID;
  rec.recordtype=AttributeRecord;
  rec.recordsize=attrib.datasize+attrib.namesize+AttributeRecordHdr::size();
  if(datasetnumber>=0) rec.sequenceID=datasetnumber;
  else rec.sequenceID=current_rec.sequenceID; // a kludge for error immunity

//  clientsock->write((char*)(&rec),sizeof(RecordHdr));
// clientsock->write((char*)(&attrib),sizeof(AttributeRecordHdr));

  RecordHdr::write(rec, clientsock);
   attrib.write(clientsock);

  clientsock->write((char*)name,stringlen);
  return clientsock->write((char*)(data),length*sizeOf(typeID));
}


