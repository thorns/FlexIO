#ifndef __SLAVEIO_HH_
#define __SLAVEIO_HH_
#include "Arch.h"

class SlaveIO {
protected:
  int fid;
public:
  SlaveIO();
  SlaveIO(char *name);
  virtual ~SlaveIO();
  virtual int writeTo(void *data,Long8 len,Long8 offset);
};

#ifdef SGI
class SGISlaveIO : public SlaveIO {
public:
  SGISlaveIO(char *name);
  // use writepos elemental operation rather than separate lseek
  virtual int writeTo(void *data,Long8 len,Long8 offset);
};

class AIOSlaveIO : public SlaveIO {
  int write_complete;
public:
  AIOSlaveIO(char *name);
  ~AIOSlaveIO();
  virtual int writeTo(void *data,Long8 len, Long8 offset);
  void sync();
};

#endif // SGI

#ifdef T3E
class FFSlaveIO : public SlaveIO {
public:
  FFSlaveIO(char *name,char *optionstring);
  FFSlaveIO(char *name,int bufsize,int numbufs);
  FFSlaveIO(char *name);
  virtual ~FFSlaveIO();
  virtual int writeTo(void *data,Long8 len,Long8 offset);
};
#endif // T3E

#ifdef SGI

class DirectSlaveIO : public SlaveIO {
  int dfid;
  int blocksize,maxiosize,memoryalignment;
  char *diobuffer;
  int diobuffersize;
public:
    DirectSlaveIO(char *name);
  virtual ~DirectSlaveIO();
  virtual int writeTo(void *data,Long8 len,Long8 offset);
};

#endif // SGI

#endif
