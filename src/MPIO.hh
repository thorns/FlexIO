#ifndef __MPIO_HH_
#define __MPIO_HH_

#include "IEEEIO.hh"
#include "MPIutils.hh"
#include "Timer.hh"

class MPIO {
typedef float *floatP;
typedef MPI_Request *MPI_RequestP;

  MPIcomm &comm;
  int *alldims,*allorigins;
  int globaldims[3]; // for all procs
  int localorigin[3],localdims[3]; // local dims are full extent + bound & ghost
  int root, myid;
  IEEEIO *file; // only on master
  MPI_Request *sendreq; // is it proper to have a pointer here?
  MPI_RequestP recvreq[2]; // double-buffered
  floatP slicebuffer[3]; // double-buffered + a scratch space
  
  
protected:
  void sendSlice(int z, float *data, MPI_Request *req);
  int requestSlice(int z, float *slicebuffer, MPI_Request *req);
  void waitForSlice(int z, float *slicebuffer, float *destbuffer, MPI_Request *req);
  void asyncWrite(float *data);
public:
  MPIO(IEEEIO *io,MPIcomm &c);
  ~MPIO();
  inline int isRoot() { return ((myid==root)?1:0);}
  void setLocalDims(int rank,int *origin,int *dims);
  virtual int write(float *data){
    asyncWrite(data);
    return 1;
  }
};

#endif
