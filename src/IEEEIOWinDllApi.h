/////////////////////////////////////////////////////////////////
//
// $Id$
//
// $Log$
// Revision 1.3  2000/09/11 14:58:45  tradke
// Undo my last commit. #ifdef _WIN32 should be fine.
//
// Revision 1.2  2000/09/11 14:07:22  tradke
// #ifdef _WIN32   ->  #ifdef WIN32
//
// Revision 1.1  2000/02/24 05:47:45  jshalf
//
// More minor bugfixes to eliminate compiler warnings.
// Added Windows support file which was formerly forgotten in
// the repos.
//
// Revision 1.1  2000/01/11 15:56:01  werner
// Windows Compliance, added WinDLL API.
//
// Revision 1.1  2000/01/07 17:52:48  bzfbenge
// Template file for Win DLL API generation
//
//
/////////////////////////////////////////////////////////////////

#ifndef __IEEEIO_WIN_DLL_API_H
#define __IEEEIO_WIN_DLL_API_H


#ifdef _WIN32
#  ifdef IEEEIO_EXPORTS
#     define IEEEIO_API __declspec(dllexport)
#  else
#     define IEEEIO_API __declspec(dllimport)
#  endif
#else
#   define IEEEIO_API 
#endif

#endif /* __IEEEIO_WIN_DLL_API_H */

