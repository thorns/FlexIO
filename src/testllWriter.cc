#include <stdio.h>
#include <stdlib.h>
#include "IO.hh"
#include "IEEEIO.hh"
#include "Reader.hh"
#include "Writer.hh"

int main(int argc,char *argv[]){
  float data[40*40*40];
  double origin[3]={0,0,0};
  double dx[3]={0.25,0.25,0.25};
  int rank=3;
  int dims[3]={40,40,40};
  //int dims[3]={1,1,1};
  char label[31];
  Writer *writer;
  IEEEIO *iefile;
  IObase *outfile = iefile = new IEEEIO("data3d.raw",IObase::Create);
  //iefile->bufferOn(128*1024); // set up a 128k write buffer
  writer = new Writer(*outfile);
  writer->setParams(rank,dims,IObase::Float32,origin,dx);
  // data[0]=1.0;
  int i;
  for(i=0;i<40*40*40;i++)
    data[i]=(float)i;
  for(i=0;i<4;i++){
    dims[2]=10+i*10;
    writer->setDims(dims);
    writer->write(data);
    //outfile->pause();
    //puts("paused");
    //outfile->resume();
    //puts("resumed");
    sprintf(label,"mylabel.%04u",i);
    //puts("after mylabel");
    outfile->writeAttribute("Mylabel",IObase::Char,31,label);
  }
  
  /* puts("\tnow seek to end\n");
  outfile->seek(outfile->nDatasets());

  puts("\treserve a chunk");
  outfile->reserveChunk(IObase::Float32,rank,dims);
  puts("\tWrite a stream fo 40x40x40");
  outfile->writeStream(data,40*40*20);
  puts("\tWrite the next stream of 40x40x20");
  outfile->writeStream(data+40*40*20,40*40*20);*/
  puts("\t writer done!!\n\n");
  
  delete writer;
  delete outfile;
  /*
  puts("Start appending");
  outfile = new IEEEIO("data3d.raw",IObase::Append);
  printf("Isvalid?  %d\n",outfile->isValid());
  
  outfile->reserveStream(IObase::Float32,rank,dims);
  outfile->writeStream(data,40*40*20);
  outfile->writeStream(data+40*40*20,40*40*20);
  printf("nrecords=%u\n",outfile->nDatasets());
  delete outfile;
  
  puts("Done appending : Reopen for reading.");
  */
  //exit(0);
  puts("start reader***************************");
  Reader *reader;
  IObase *infile = new IEEEIO("data3d.raw",IObase::Read);
  //printf("nrecords=%u\n",outfile->nDatasets());
  // reader = new Reader(*infile);
  int ndatasets=infile->nDatasets();
  printf("Number of datasets=%d\n",ndatasets);
  for(i=0;i<ndatasets;i++){
    int j,r;
    float buffer[40*40*40];
    for(j=0;j<40*40*40;j++) buffer[j]=0;
    int rank,dims[3];
    IObase::DataType datatype;

    printf("Cycle %u\n",i);
    infile->readInfo(datatype,rank,dims);
    printf("Data info dt=%u,rank=%u,dims=%u,%u,%u\n",
	   (int)(datatype),rank,dims[0],dims[1],dims[2]);
    //r=infile->read(buffer);
    r=infile->readStream(buffer,dims[0]*dims[1]*dims[2]);
    printf("read %d elements\n",r);
    for(j=0;j<10;j++)
      printf("Data[%u]=%f\n",j,buffer[j]);
    puts("***done printing data");
    int nattribs=infile->nAttributes();
    printf("Number of Attributes=%d\n",nattribs);
    for(j=0;j<nattribs;j++){
      char attribname[64];
      Long length;
      infile->readAttributeInfo(j,attribname,datatype,length);
      printf("\tAttribute[%u] name=%s\n",j,attribname);
      switch(datatype){
      case IObase::Float32:{
	//float buf[5];
	puts("Float");
	printf("length=%u\n",length);
      } break;
      case IObase::Float64:{
	double buf[5];
	puts("Double");
	printf("length=%u\n",length);
	infile->readAttribute(j,buf);
	for(int k=0;k<length;k++)
	  printf("\tElement[%u]=%lf\n",k,buf[k]);
      } break;
      case IObase::Int32:{
	//int buf[5];
	puts("Integer");
	printf("length=%u\n",length);
      } break;
      case IObase::String:{
	char buf[128];
	puts("String");
	infile->readAttribute(j,buf);
	printf("len=%u val=[%s]\n",length,buf);
	break;
      }
      default:
	printf("unknown type %d\n",(int)datatype);
	break;
      }
    }
  }
  //delete reader;
  delete infile;
  puts("done");
  return 0;
}
