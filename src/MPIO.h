#ifndef __MPIO_H_
#define __MPIO_H_

#include "Arch.h"

/*********************C Interface**********************/

IOFile MPIOopen (IOFile fid);

int MPIOclose (IOFile deviceID);

void MPIOsetLocalDims (IOFile deviceID, int rank, int *origin, int *dims);

int MPIOwrite (IOFile deviceID, int typeID, void *data);

#endif
