#ifndef __IEEEIOSLAVE_HH_
#define __IEEEIOSLAVE_HH_

class IEEEIOslave {
  // open two fid's (one for O_DIRECT and the other for buffered)
  // determine which sections of the buffer are remainders and
  // write them buffered which requires nasty locking
  // since this is SGI_specific, we can use aio for the remainders
  int fid;
  struct ffsw ffopens_status;
public:
  IEEEIOslave(char *filename):fid(-1){
    // opens read/write
    fid = open(fname,O_RDWR,0644);
  }
  ~IEEEIOslave(){
    if(fid>=0){
      close(fid);
    }
  }
};

#ifdef T3E
// T3E-specific ffio slaves
// #define open(x,y,z) ffopens(x,y,z, 0, 0, &ffopens_status, "bufa.bufsize=256.num_buffers=4")

class ffIEEEIOslave {
    int fid;
public:
  IEEEIOslave(char *filename):fid(-1){
    // opens read/write
    fid = ffopens(fname,O_RDWR,0644);
  }
  ~IEEEIOslave(){
    if(fid>=0){
      ffclose(fid);
    }
  }
};



#ifdef SGI
// SGI-specific slaves

class asyncIEEEIOslave {
    int fid;
public:
  IEEEIOslave(char *filename):fid(-1){
    // opens read/write
  }
  ~IEEEIOslave(){
    if(fid>=0){
      close(fid);
    }
  }
};



#endif

#endif
