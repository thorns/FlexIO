#include <stdio.h>
#include <stdlib.h>
#include "IEEEIO.hh"
#include "vatoi.hh"
#include <sys/time.h>
double MPI_Wtime(){
  timeval tv;
  gettimeofday(&tv,0);
  return tv.tv_sec + tv.tv_usec/1000000.0l;
}

class CommandLine {
public:
  int dims[3];
  char *programname;
  int bufsize;
  int ntrials;
  void setDefaults(){
    dims[0]=dims[1]=dims[2]=128;
    bufsize=0;
    ntrials=40;
  }
  CommandLine(int argc,char *argv[]){
    setDefaults();
    parse(argc,argv);
  }
  void parse(int argc,char *argv[]){
    programname=argv[0];
    for(int i=1;i<argc;i++){
      char *flag = argv[i],*val=argv[i+1];
      if(*flag=='-') flag++;
      else continue; // ignore non-flags
      if(!strcmp(flag,"dims")){
	if(++i<argc) sscanf(val,"%u,%u,%u",dims,dims+1,dims+2);
      }
      else if(!strcmp(flag,"buffer")) {
	if(++i<argc)
	  bufsize = vatoi(val);
      }
      else if(*flag=='n') {
	ntrials=atoi(val);
	i++;
      }
      else if(*flag=='h') usage();
    }
  }
  void usage(){
    printf("Usage: %s <-dims int,int,int> -n <ntrials>\n",
	   programname);
    printf("\tdims: Set the dimensions of the dataset.\n");
    printf("\tn: Set number of trials of writing the dataset.\n");
    printf("\tbuffer: Set buffer cache size in bytes.\n");
  }
  void printStatus(FILE *f=stdout){
    fprintf(f,"%s: dims=%u,%u,%u\n",
	   programname,
	   dims[0],dims[1],dims[2]);
  }
};

int main(int argc,char *argv[]){
  CommandLine cmdln(argc,argv);

  int size=IObase::nElements(3,cmdln.dims);
  int i;
  float *data = new float[size];
  for(i=0;i<size;i++) data[i] = (float)i;
  IEEEIO *file = new IEEEIO("rawdata.raw",IObase::Write);
  if(cmdln.bufsize>0) file->bufferOn(cmdln.bufsize);
  double stime = MPI_Wtime();
  for(i=0;i<cmdln.ntrials;i++){ // do this 40 times
    file->reserveStream(IObase::Float32,3,cmdln.dims);
    file->writeStream(data,size);
  }
  if(file) delete file;  // make sure the file is deleted
  double etime = MPI_Wtime();
  printf("Elapsed time to write=%lf\n",etime-stime);
  printf("IO performance = %f Megabytes/sec\n",
	 (float)(IObase::nBytes(IObase::Float32,3,cmdln.dims))*(float)(cmdln.ntrials)/(1024*1024*(etime-stime)));
  delete data;
}

  
