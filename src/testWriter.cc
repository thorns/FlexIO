#include <stdio.h>
#include <stdlib.h>
#include "IO.hh"
#include "IEEEIO.hh"
// #include "Reader.hh"
#include "Writer.hh"

int main(int argc,char *argv[]){
  float data[40*40*40];
  double origin[3]={0,0,0};
  double dx[3]={0.25,0.25,0.25};
  int i,rank=3;
  int dims[3]={40,40,40};
  Writer *writer;
  IObase *outfile = new IEEEIO("data3d.raw",IObase::Create);
  writer = new Writer(*outfile);
  writer->setParams(rank,dims,IObase::Float32,origin,dx);
  // data[0]=1.0;
  for(i=0;i<40*40*40;i++)
    data[i]=(float)i;
  for(i=0;i<2;i++)
    writer->write(data);

  delete writer;
  delete outfile;
#if 0
  Reader *reader;
  IObase *infile = new IEEEIO("data3d.raw",IObase::Read);
  reader = new Reader(*infile);
  
  for(i=0;i<reader->ndatasets;i++){
    int j;
    float buffer[40*40*40];
    for(j=0;j<40*40*40;j++) buffer[j]=0;
    (*reader)[i].read(buffer);
    for(j=0;j<10;j++)
      printf("Data[%u]=%f\n",j,buffer[j]);
    puts("***done printing data");
    for(j=0;j<(*reader)[i].nattributes;j++){
      //IOdataset ds=(*reader)[i];
      //char c1,c2;
      //int attrlen;
      //printf("\tnattribs=%u\n",ds.nattributes);
      //c1=ds.attribute[j].name[0];
      //c2=ds.attribute[j].name[0];
      //printf("\tFirst two chars of name [%c][%c]\n",c1,c2);
      //attrlen=ds.attribute[j].nelements;
      printf("\tAttribute[%u] name=%s\n",j,(*reader)[i].attribute[j].name);
      long length=(*reader)[i].attribute[j].nelements;
      switch((*reader)[i].attribute[j].datatype){
      case IObase::Float32:{
	//float buf[5];
	puts("Float");
	printf("length=%u\n",length);
      } break;
      case IObase::Float64:{
	double buf[5];
	puts("Double");
	printf("length=%u\n",length);
	(*reader)[i].attribute[j].read(buf);
	for(int k=0;k<length;k++)
	  printf("\tElement[%u]=%lf\n",k,buf[k]);
      } break;
      case IObase::Int32:{
	//int buf[5];
	puts("Integer");
	printf("length=%u\n",length);
      } break;
      default:
	puts("unknown type");
	break;
      }
    }
  }
  delete reader;
  delete infile;
#endif
  puts("done");
  return 0;
}
