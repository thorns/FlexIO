#include "vatoi.hh"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int vatoi(char *p){
  char param[128];
  strcpy(param,p);
  char *offset;
  int n;
  if((offset=strchr(param,'k')) || (offset=strchr(param,'K')) ){
    *offset='\0';
    n=1024;
  }
  else if((offset=strchr(param,'m')) || (offset=strchr(param,'M')) ){
    *offset='\0';
    n=1024*1024;
  }
  else n=1;
  n*=atoi(param);
  return n;
}
