#include "AMRwriter.hh"
#include "AMRgridPlus.h"

/*
 * AMRwriterPlus:
 * A subclass of AMRwriter which adds a scalar range attribute
 * to each dataset. The AMRwriter interface is unchanged,  however
 * for convenience,  you can:
 * 
 * Set the top level parameters or write a dataset by passing a
 * pointer to an AMRgridPlus datastructure to ::setTopLevelParameters
 * or ::write respectively
 * 
 * Set the scalar range of the dataset explicitly (before writing)
 * with ::setScalarRange. If this is not called,  the scalar range will
 * be computed from within ::write. Useful (and timesaving) if you
 * have already computed the range.
 */

class AMRwriterPlus : public AMRwriter{
    public:
    AMRwriterPlus(IObase &descriptor);
    
    virtual void setTopLevelParameters(AMRgridPlus *g){
	AMRwriter::setTopLevelParameters(g->rank, g->origin, g->delta,  \
	    g->timestep, g->maxlevel);
    }
    
    // CC forces me to re-overload this inherited method!
    virtual void setTopLevelParameters(int rank,double *origin,
            double *delta,double timestep,int maxdepth){
	AMRwriter::setTopLevelParameters(rank, origin, delta, timestep, maxdepth);
    }
    
    void setScalarRange(double smin,double smax){
	curscalarmin=smin;curscalarmax=smax;scalarFlag=1;}

    void write(int *origin,int *dims,void *data){
	AMRwriter::write(origin, dims, data);
	if (!scalarFlag) calcScalarRange(data);
	writePlusattributes();
    } 
    void write(float *origin,int *dims,void *data){
	AMRwriter::write(origin, dims, data);
	if (!scalarFlag) calcScalarRange(data);
	writePlusattributes();
    }
    void write(double *origin,int *dims,void *data){
	AMRwriter::write(origin, dims, data);
	if (!scalarFlag) calcScalarRange(data);
	writePlusattributes();
    }
    
    virtual void write(AMRgridPlus *g, int calcscalars=0){
	
	setType((IObase::DataType)g->datatype);
	setLevel(g->level);
	setTime(g->timestep);
	write(g->origin, g->dims, g->data);
    }
    
    protected:
    void writePlusattributes();
    
    private:
    void calcScalarRange(void *data);
    
    double curscalarmin, curscalarmax;
    int scalarFlag;
};
