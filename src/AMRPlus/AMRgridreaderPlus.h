#ifndef __AMRGRIDREADERPLUS_HH_
#define __AMRGRIDREADERPLUS_HH_

#include <IO.hh>
#include "AMRgridPlus.h"
#include "IEEEIO.hh"


class AMRgridreaderPlus {
protected:
    IObase &file;
  
public:
    AMRgridreaderPlus(IObase &f) : file(f){};
    ~AMRgridreaderPlus(){};
  
    /* 
     * Low level grid/info fetching routines 
     */
    AMRgridPlus *getGrid(AMRgridPlus &g,int index){
	if(file.seek(index)<index)
	return 0; // don't load past end
	getGridInfo(g,index);
	getGridData(g,index);
	return &g;
    }
    AMRgridPlus *getGrid(int index){
	AMRgridPlus *g=new AMRgridPlus;
	return this->getGrid(*g,index);
    }
  
    AMRgridPlus *getGridInfo(AMRgridPlus &g,int index);
    AMRgridPlus *getGridData(AMRgridPlus &g,int index);


 

};


#endif 
