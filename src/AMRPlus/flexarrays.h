#ifndef ARRAYS_HDR
#define ARRAYS_HDR
#include <string.h>
#include <stdlib.h>
//For Shared Objects:
//#include "SafeList.h"
//#define TAG :public shObj
//#define GetMem CAVEMalloc
//#define FreeMem CAVEFree
//For Non-shared Objects:
#define TAG 

/* An flexarray is a dynamic flexarray of objecs. No upper limit, 
 * the ability to add and remove objects on the fly,  etc.
 * 
 * Rundown of routines:
 * Constructor - initializes an empty flexarray with increment 10
 * Destructor - Deletes the flexarray. Does NOT delete objects in the flexarray
 * copy constructor/ = :deep copy (override X::operator= if you need
 *	deep copies of the elements)
 * 
 * Information:
 * int getLength()/getSize() - returns #objects
 * int getActualSize() - returns #slots allocated
 * int getIncrement() - returns #slots allocated at a time
 * int empty() - true if no elts
 * 
 * Access:
 * X& [idx] - return indexed reference,  unsafe
 * X* getData(idx) - Ptr to indexed elt.
 * X* getDataConst - same as above,  but everything is const qualified
 *
 * Manip: 
 * setIncrement(int) - 
 * clear() - empties the array
 * fill(X&, N) - sets the array to N copies of the X
 * setData(X&, idx) - change entry at index
 * insertElement(X&, idx) - insert element BEFORE index 
 *	(if index=-1,  append element);
 * append(X&) - duh.
 * InsertElements(start,  X*, N) - insert  #elements before
 *	start... read values from X*.
 * removeElements(start, N) - remove N elements starting with start
 */

template <class X>
class flexarray TAG {
    int Length, Size, Increment;
    X* data;
    
    public:
    flexarray(){Length=Size=0; Increment=10;data=NULL;}
    flexarray(const flexarray<X> &src){
	Length=src.Length;Size=src.Size;Increment=src.Increment;
	if (Size==0) data=NULL;
	else {
	    data=getNewArray(Size);
	    memcpy(this->data, src.data, this->Length*sizeof(X));
	}
    }	
    ~flexarray(){if (data) delete[] data;}
    
    flexarray<X> &operator=(const flexarray<X> &src){
	if (data!=NULL) delete[] data;
	Length=src.Length;Size=src.Size;Increment=src.Increment;
	if (Size==0) data=NULL;
	else {
	    data=getNewArray(Size);
	    memcpy(this->data, src.data, this->Length*sizeof(X));
	}
	return *this;
    }
    
    int getActualSize() const{return Size;}
    int getSize() const{return Length;}
    int getLength() const{return Length;}
    int getIncrement() const{return Increment;}
    int empty() const{return (Size==0);}
    
    
    
    const X & operator()(int idx) const{return data[idx];}
    X & operator[](int idx) const{return data[idx];}
    
    X* getData(int idx)const{
	if (idx==-1) idx=Length-1;
	if (idx>=0 && idx<Length)
	    return data+idx;
	else return NULL;
    }
    X* getDataConst(int idx) const{
	if (idx==-1) idx=Length-1;
	if (idx>=0 && idx<Length)
	    return data+idx;
	else return NULL;
    }
    
    void setIncrement(int ninc){Increment=ninc;}
    void clear(){Length=Size=0; if(data) delete[] data;data=NULL;}
    void fill(const X &elt, int howmany){
	clear();
	Length=howmany;
	Size=Increment * ((Length+howmany)/Increment +1);
	data=getNewArray(Size);
	for (int ii=0;ii<howmany;ii++)
	    data[ii]=elt;
    }
    
    void setData(int idx, const X &ndata){
	if (idx>=0 && idx<Length)
	    data[idx]=ndata;
    }
    
    
    
    
    void insertElement( const X &elt, int atidx=-1){
	insertElements(atidx,  &elt, 1);
    }
    void append( const X &elt){
	insertElements(-1,  &elt, 1);
    }
    
    
    void insertElements(int startidx, const X* elts, int howmany){
	if (howmany<=0) return;
	if (startidx<-1 || startidx>Length) return;
	if (startidx==-1) startidx=Length;
	if (this->data==NULL) data=getNewArray(Increment);
	if (Size<Length+howmany){
	   Size=Increment * ((Length+howmany)/Increment +1);
	   X* tflexarray=getNewArray(Size);
	   if (startidx>0)
		memcpy(tflexarray, data, startidx*sizeof(X));
	    if (startidx<Length)
		memcpy(tflexarray+startidx+howmany, data+startidx, \
		    ((Length-startidx)*sizeof(X)));
	    delete[] data;
	    data=tflexarray;
	}
	else
	    if (startidx<Length)
		memcpy(data+startidx+howmany, data+startidx, \
		    (Length-startidx)*sizeof(X));
	memcpy(data+startidx, elts, howmany*sizeof(X));
	Length+=howmany;
    }
    
    void removeElements(int startidx, int howmany){
	if (howmany<=0 || startidx<-1 || startidx>=Length) return;
	if (startidx==-1) startidx=Length-1;
	if (startidx+howmany>=Length) howmany=Length-startidx;
	memcpy(data+startidx, data+startidx+howmany, 
	    (Length-(startidx+howmany))*sizeof(X));
	Length-=howmany;
    }
       
    
    private:
    X* getNewArray(int newSize){
	return new X [newSize];
    }
    
    void resizeArray(int startidx, int numelts){
	int NSize=Length+numelts;
	if (NSize>Size){
	    Size=Increment * (NSize/Increment +1);
	    X* tflexarray=getNewArray(Size);
	    if (startidx>0)
		memcpy(tflexarray, data, startidx*sizeof(X));
	    if (startidx<Length)
		memcpy(tflexarray+startidx+numelts, data+startidx, \
		    ((Length-startidx)*sizeof(X)));
	    delete[] data;
	    data=tflexarray;
	}
    }
    
    
    
};


#endif
