#ifndef AMR_TREE_H
#define AMR_TREE_H
#include "FlexArrayTempl.H"
#include "AMRgridPlus.h"

struct idxrec {
    int idx;
    int datastat;
};

typedef FlexArray<int> IntArray;

typedef FlexArray<AMRgridPlus> GridArray

typedef FlexArray<idxrec> IdxArray;

class AMRTree;
struct AMRNode{
    IdxArray idxs;
    AMRTree subtimes;
};

class AMRTree:  protected FlexArray<AMRNode>{
    void buildtree(AMRTree *t, int &idx);
    public:
    AMRTree(GridArray *);
    ~AMRTree();
    
    private:
    AMRNode timenode;
    GridArray grids;
};


#endif
