#ifndef __AMRREADERPLUS_HH_
#define __AMRREADERPLUS_HH_

#include <set>
#include <vector>
#include <IO.hh>
#include "AMRgridreaderPlus.h"
#include "IEEEIO.hh"

#include "flexmatrix.h"
#include "flexset.h"
#define kNoData 0
#define kHasData 1

/* Method for calculating scalar range - the global range,
range per fine timestep, per coarse, or per (current) root. */
typedef enum {kGlobal,kFine,kCoarse,kRoot} ScalarMode;

using namespace std;



typedef flexarray<int> IntArray;



typedef flexarray<int> IdxArray;


class AMRreaderPlus : public AMRgridreaderPlus {
    public:
    int datatype;
    int numgrids;
    int maxlevel,maxtimeres,maxtime;
    double smin,smax;
    int modflag;
    
    IdxSet activeset;
    flexset<float> realtimes;
    flexmatrix<IdxArray> *leveltimes;
    GridArray grids;
    IntArray levelmask, timemask;
    
    void init();
    void build_info();
    void build_active();
    void modify();	//Flag that tells when active set has changed
public:
    AMRreaderPlus(IObase &f);
    ~AMRreaderPlus();
    
    int getNumLevels(){return maxlevel+1;}
    int getNumTimesteps(){return maxtime+1;}
    int getNumGrids(){return numgrids;}
    int getDataType(){return datatype;}
    
    void showTimeStep(int ts){
	if (ts>=0 && ts<=maxtime) {timemask[ts]=1; modify();}
	else cout<<"Timestep out of range: "<<ts<<endl;
    }
    void hideTimeStep(int ts){
	if (ts>=0 && ts<=maxtime) {timemask[ts]=0; modify();}
	else cout<<"Timestep out of range: "<<ts<<endl;
    }
    void selectTimeStep(int ts) //Shortcut- hides all times but ts
    { for (int ii=0;ii<=maxtime;ii++) timemask[ii]=(ii==ts)?1:0;modify();}
    
    void showLevel(int lev){
	if (lev>=0 && lev<=maxlevel) {levelmask[lev]=1; modify();}
	else cout<<"Level out of range: "<<lev<<endl;
    }
    void hideLevel(int lev){
	if (lev>=0 && lev<=maxlevel) {levelmask[lev]=0; modify();}
	else cout<<"Level out of range: "<<lev<<endl;
    }
    
    GridArray *getGridSet(){return &grids;}
    void getActive(IdxSet &grids);
    void loadData(IdxSet &loadset);
    void loadData(){loadData(activeset);}
    
};


#endif 
