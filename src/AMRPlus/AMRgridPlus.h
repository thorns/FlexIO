#ifndef __AMR_GRID_HH_
#define __AMR_GRID_HH_
#include "AmrGrid.h"
#include "flexset.h"

struct AMRgridPlus : public AmrGrid {
  double time;
  int timeref, spaceref[3], placeref[3];
  double scalarmin,scalarmax;
};

struct idxrec{
    int idx;
    idxrec(int first=0){idx=first;}
    int operator<(const idxrec &other)const {return idx<other.idx;}
    int operator==(const idxrec &other)const {return idx==other.idx;}
};

typedef flexset<idxrec> IdxSet;
typedef flexarray<AMRgridPlus > GridArray;
#endif
