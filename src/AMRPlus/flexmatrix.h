#ifndef FLEX_MATRIX_H
#define FLEX_MATRIX_H
#include <stdlib.h>

template <class X>
class flexmatrix{
    X* data;
    int M, N;
    
    public:
    flexmatrix(int m, int n){
	M=m;N=n; if (M*N==0) cout<<"Serious Problem! "<<endl;
	data=new X [M*N];
    }
    ~flexmatrix(){delete[] data;}
    X& operator()(int m, int n){return data[m*N+n];}
};


#endif
