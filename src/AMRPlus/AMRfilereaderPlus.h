// AmrFileReader
#ifndef __AMRFILEREADERPLUS_HH_
#define __AMRFILEREADERPLUS_HH_
#include <stdio.h>
#include <IO.hh>
#include "AMRgridreaderPlus.h"
#include "FlexArrayTmpl.H"

class AMRfilereaderPlus : public AMRgridreaderPlus {
protected:
  int gridloading;
  FlexArray<int> activeGrids;
  FlexArray<AMRgridPlus> grids; 
  FlexArray<int> levelmask;
  IObase::DataType datatype;
  int maxlevel,maxtimeres,mintime,maxtime;
  double smax, smin;
  double bounds[6];
  int current_time;
  // Internal Utility methods
  void buildInfoTable();
  void loadGrids();
  void reclaimGrids();
  void purgeGrids();
  void printGridInfo(AMRgridPlus &g);
public:
  int debug;
  void printGridInfo();
  void printActiveGrids();
  AMRfilereaderPlus(IObase &f);
  int getNumLevels(){ return maxlevel+1; }
  void getTimeRange(int &min,int &max){
    min=mintime;
    max=maxtime;
  }
  void getScalarRange(float &min, float &max){
      min=smin;max=smax;
  }
  void getScalarRange(double &min, double &max){
      min=smin;max=smax;
  }
  void getBounds(double *bnds){for (int ii=0;ii<6;ii++){bnds[ii]=bounds[ii];}}
  void getBounds(float *bnds){for (int ii=0;ii<6;ii++){bnds[ii]=bounds[ii];}}
  void setTime(int timestep);
  // starts out with all selected
  void showLevel(int level=-1){ // default is all (-1)
    if(level>=levelmask.getSize() || level<0){
      printf("AmrConvert::showLevel(%u) :  Level out of range 0:%u\n",
	     level,levelmask.getSize()-1);
    }
    else
      levelmask[level]=1;
  }
  void showAllLevels();
  void hideAllLevels(){for (int ii=0;ii<maxlevel+1;ii++)hideLevel(ii);}
  void hideLevel(int level=-1){  // default is all (-1)
    if(level>=levelmask.getSize() || level<0){
      printf("AmrConvert::showLevel(%u) :  Level out of range 0:%u\n",
	     level,levelmask.getSize()-1);
    }
    else
      levelmask[level]=0;
  }
  int nLevels(){ return maxlevel+1; }
  IObase::DataType getDataType(){return datatype;}
  // For C interface
  int getNumGrids(){ // number of active grids
    return activeGrids.getSize();
  }
  int getActiveIndex(int ii){ return activeGrids[ii];}
  int getGrids(AMRgridPlus *g);
  // For C++ interface
  int getGrids(FlexArray<AMRgridPlus> &g);
  void setDataLoadingOff(){ gridloading=0; purgeGrids();}
  void setDataLoadingOn(){ gridloading=1; loadGrids();}
};

#endif
