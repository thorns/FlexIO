#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "IO.hh"
#include "IEEEIO.hh"

#ifdef WITH_HDF4
#include "HDFIO.hh"
#endif

#ifdef WITH_HDF5
#include "H5IO.hh"
#endif

// dummy routine
extern "C" void CCTKi_RegisterThorn() {}

char *programname;
void usage(){
  printf("Correct usage is %s <infile> <outfile> [-swapbytes]\n",programname);
  puts("-swapbytes requests opposite of native byte-order for IEEEIO output");
  puts("The default is to write IEEE files in native byte-order");
  exit(1); // failure code
}

int main(int argc,char *argv[]){
  programname=argv[0]; // for usage/error_exit routine
  if(argc<3) usage();
  int i,swap;
  int sindex=0,dindex=0,findex=0;
  IObase *infile = NULL,*outfile = NULL;
#ifdef WITH_HDF4
  HDFIO *hdffile;
#endif
  int hdfin=0;
  int h5in=0;
  for(i=1;i<argc;i++){
    if(*(argv[i])=='-') findex=i;
    else if(!sindex) sindex=i;
    else dindex=i;
  }
  if(!sindex || !dindex || // doesn't have both infile and outfile specified
     (findex && !strcmp("-help",argv[findex])) || // used help flag
     (findex && strcmp("-swapbytes",argv[findex])) )  // invalid flag
    usage();
  if(findex && !strcmp("-swapbytes",argv[findex])) swap=1;
  else swap=0;

  // for the input file
  char *s_ext=strrchr(argv[sindex],'.');
  char *d_ext=strrchr(argv[dindex],'.');
  if(s_ext && (!strcmp(s_ext,".ieee") || !strcmp(s_ext,".raw"))){
    infile=new IEEEIO(argv[sindex],IObase::Read);
  }
#ifdef WITH_HDF4
  else if(s_ext && !strcmp(s_ext,".hdf")){
    infile=hdffile=new HDFIO(argv[sindex],IObase::Read);
    hdfin=1;
  }
#endif
#ifdef WITH_HDF5
  else if(s_ext && (!strcmp(s_ext,".h5") || !strcmp(s_ext,".hdf5"))){
    infile = new H5IO(argv[sindex],IObase::Read);
    if(!infile->isValid()) fprintf(stderr,"**** not hdf5 input?\n");
    h5in=1;
  }
#endif
  if(!infile->isValid()){
    printf("cant read %s\n",argv[sindex]); 
    usage();
  }

  // for the outfile,  
  if(d_ext && (!strcmp(d_ext,".ieee") || !strcmp(d_ext,".raw"))){
    outfile=new IEEEIO(argv[dindex],IObase::Create,swap); // swap only affects IEEEIO output
  }
#ifdef WITH_HDF4
  else if(d_ext && !strcmp(d_ext,".hdf")){
    outfile=new HDFIO(argv[dindex],IObase::Create);
  }
#endif
#ifdef WITH_HDF5
  else if(d_ext && (!strcmp(d_ext,".h5") || !strcmp(d_ext,".hdf5"))){
    outfile = new H5IO(argv[dindex],IObase::Create);
  }
#endif
    
  if(! (outfile && outfile->isValid())){
    printf("cant write %s\n",argv[dindex]); 
    delete infile; 
    usage();
  }

  // Now do the conversion
  for(i=0;i<infile->nDatasets();i++){
    int rank,dims[5];
    IObase::DataType type;
    char *data;
    infile->seek(i);
#ifdef WITH_HDF4
    if(hdfin && hdffile->isCoord()) continue; // skip coord vars
#endif
    fprintf(stderr,".%u",i);
    infile->readInfo(type,rank,dims);
    {
      int j,sz;
      for(j=0,sz=1;j<rank;j++) sz*=dims[j];
      sz*=IObase::sizeOf(type);
      data=new char[sz];
    }
    infile->read(data);
    outfile->write(type,rank,dims,data);
    delete data; // OK, so I'm not being smart about mem allocation here
    for(int j=0;j<infile->nAttributes();j++){
      char name[256]; // should be big enough
      int length;
      fprintf(stderr,"a");
      infile->readAttributeInfo(j,name,type,length);
      data = new char[length * IObase::sizeOf(type)];
      infile->readAttribute(j,data);
      outfile->writeAttribute(name,type,length,data);
      delete data;
    }
  }
  puts("");
  delete outfile;
  delete infile;
  return 0; // success code
}
