#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <IO.hh>
#include "WriteHLL.hh"

// declare a new dataset
void WriteHLL::newDataset(int ndims,long npoints){
  int dt=1;
  current_ndims=ndims;
  file.write(IObase::Int32,dt,&dt,&ndims); // single integer (ndims)
  switch(ndims){
  case 1:
    current_cellsize=2;
    current_celltype="line";
    break;
  case 2:
    current_cellsize=4;
    current_celltype="quad";
    break;
  case 3:
    current_cellsize=8;
    current_celltype="hex";
    break;
  default:
    fprintf(stderr,"WriteHLL:newDataset(), ndims=%d is invalid\n",ndims);
    break;
  }
  current_npoints=npoints;
  file.writeAttribute("cellsize",IObase::Int32,1,&current_cellsize);
  file.writeAttribute("celltype",IObase::String,strlen(current_celltype),current_celltype);
}
// each array is npoints long
void WriteHLL::writeCoords(void *xcoords,void *ycoords,void *zcoords){
  if(xcoords)
  file.writeAttribute("xcoords",IObase::Float32,current_npoints,xcoords);
  if(ycoords)
  file.writeAttribute("ycoords",IObase::Float32,current_npoints,ycoords);
  if(zcoords)
  file.writeAttribute("zcoords",IObase::Float32,current_npoints,zcoords);
}

void WriteHLL::writeCoordsXYZ(void *xyzcoordsp){ // this is npoints *3 long
  register long s,d,last=current_npoints*3;
  float *xyzcoords=(float*)xyzcoordsp;
  char coordname[32];
  float *coords = new float[current_npoints];
  strcpy(coordname,"xcoords");
  for(int i=0;i<current_ndims;i++,(*coordname)++){
    for(s=i;s<last;s+=current_ndims,d++) coords[d]=xyzcoords[s];
    file.writeAttribute(coordname,IObase::Float32,current_npoints,coords);
  }
  delete coords;
}
void WriteHLL::writeConnectivity(void *cellsp,long ncells){
  ncells=(ncells<0)?current_npoints:ncells;
  int *cells=(int*)cellsp;
  file.writeAttribute("cells",IObase::Int32,ncells*current_cellsize,cells);
}
// npoints data points since the data is vertex-centered
// each dataset can be of independent type
void WriteHLL::writeData(char *dataname,IObase::DataType datatype,void *data,int veclen){
  char dname[128];
  sprintf(dname,"data_%s",dataname);
  file.writeAttribute(dname,datatype,current_npoints*veclen,data);
}
// declare a new dataset
void ReadHLL::selectDataset(int step,int &ndims,int &npoints,int &ncells,int &ndata){
  IObase::DataType dt;
  int index,dims,len;
  file.seek(step);
  file.readInfo(dt,index,&dims);
  file.read(&ndims);
  file.readAttributeInfo("xcoords",dt,npoints);
  file.readAttributeInfo("cells",dt,ncells);
  index=file.readAttributeInfo("cellsize",dt,len);
  file.readAttribute(index,&current_cellsize);
  ncells/=current_cellsize;

  current_ndims=ndims;
  current_npoints=npoints;
  current_ncells=ncells;
}
void ReadHLL::readDataInfo(char *names[],
			  IObase::DataType *datatypes,
			  int *veclens){
  for(int i=0,dataindex=0,nannotations=file.nAnnotations();
      i<nannotations;i++){
    char name[128];
    file.readAttributeInfo(i,name,datatypes[dataindex],veclens[dataindex]);
    if(!strncmp(name,"data_",5)){
      strcpy(names[dataindex],name+5);
      veclens[dataindex]/=current_npoints;
      dataindex++;
    }
  }
}
void ReadHLL::readDataInfo(char *names[],
			  int *datatypes,
			  int *veclens){
  for(int i=0,dataindex=0,nannotations=file.nAnnotations();
      i<nannotations;i++){
    char name[128];
    IObase::DataType dt;
    file.readAttributeInfo(i,name,dt,veclens[dataindex]);
    datatypes[dataindex]=dt;
    if(!strncmp(name,"data_",5)){
      strcpy(names[dataindex],name+5);
      veclens[dataindex]/=current_npoints;
      dataindex++;
    }
  }
}
// each array is npoints long
void ReadHLL::readCoords(float *xcoords,float *ycoords,float *zcoords){
  IObase::DataType dt;
  int npoints,idx;
  if(current_ndims>=1 && xcoords)
    idx=file.readAttributeInfo("xcoords",dt,npoints);
  if(idx>=0 && npoints==current_npoints)
    file.readAttribute(idx,xcoords);
  if(current_ndims>=1 && ycoords)
    idx=file.readAttributeInfo("ycoords",dt,npoints);
  if(idx>=0 && npoints==current_npoints)
    file.readAttribute(idx,ycoords);
  if(current_ndims>=1 && zcoords)
    idx=file.readAttributeInfo("zcoords",dt,npoints);
  if(idx>=0 && npoints==current_npoints)
    file.readAttribute(idx,zcoords);
}
// will assume ncells=npoints unless overridden.
void ReadHLL::readConnectivity(int *cells){
  int arraysize,idx;
  IObase::DataType dt;
  idx=file.readAttributeInfo("cells",dt,arraysize);
  file.readAttribute(idx,cells);
}

// npoints data points since the data is vertex-centered
// each dataset can be of independent type
void ReadHLL::readData(char *name,void *data){
  char dname[128];
  IObase::DataType datatype;
  int len,idx;
  sprintf(dname,"data_%s",name);
  idx=file.readAttributeInfo(dname,datatype,len);
  file.readAttribute(idx,data);
}



IOFile *HLLnewWriter(IOFile *file){
  IObase *iofile = (IObase *)file;
  return (IOFile *)(new WriteHLL(*iofile));
}
void HLLdeleteWriter(IOFile *file){
  if(!file) return;
  WriteHLL *h = (WriteHLL *)file;
  delete h;
}

void HLLsetCoordType(IOFile *handle,int coordtype){
  WriteHLL *h = (WriteHLL *)handle;
  h->setCoordType(IObase::Int2DataType(coordtype));
}

void HLLsetIndexType(IOFile *handle,int indextype){
  WriteHLL *h = (WriteHLL *)handle;
  h->setIndexType(IObase::Int2DataType(indextype));
}

void HLLnewDataset(IOFile *handle,int ndims,long npoints){
  WriteHLL *h = (WriteHLL *)handle;
  h->newDataset(ndims,npoints);
}
void HLLwriteCoords(IOFile *handle,
		    float *xcoords,float *ycoords,float *zcoords){
  WriteHLL *h = (WriteHLL *)handle;
  h->writeCoords(xcoords,ycoords,zcoords);
}
void HLLwriteCoordsXYZ(IOFile *handle,float *xcoords){
  WriteHLL *h = (WriteHLL *)handle;
  h->writeCoordsXYZ(xcoords);
}
void HLLwriteConnectivity(IOFile *handle,int *cells,int ncells){
  WriteHLL *h = (WriteHLL *)handle;
  h->writeConnectivity(cells,ncells);
}
void HLLwriteData(IOFile *handle,char *dataname,int datatype,
		  void *data,int veclen){
  WriteHLL *h = (WriteHLL *)handle;
  h->writeData(dataname,IObase::Int2DataType(datatype),data,veclen);
}

IOFile *HLLnewReader(IOFile *base){
  IObase *iofile = (IObase *)base;
  return (IOFile *)(new ReadHLL(*iofile));
}
void HLLdeleteReader(IOFile *handle){
  if(!handle) return;
  ReadHLL *h = (ReadHLL *)handle;
  delete h;
}

void HLLselectDataset(IOFile *handle,
		      int index,int *ndims,int *npoints,
		      int *ncells,int *ndata){
  ReadHLL *h = (ReadHLL *)handle;
  h->selectDataset(index,*ndims,*npoints,*ncells,*ndata);
}
void HLLreadDataInfo(IOFile *handle,char *names[],
		     int *datatypes,int *veclens){
  ReadHLL *h = (ReadHLL *)handle;
  h->readDataInfo(names,datatypes,veclens);
}
void HLLreadCoords(IOFile *handle,
		   float *xcoords,float *ycoords,float *zcoords){
  ReadHLL *h = (ReadHLL *)handle;
  h->readCoords(xcoords,ycoords,zcoords);
}
void HLLreadConnectivity(IOFile *handle,int *cells){
  ReadHLL *h = (ReadHLL *)handle;
  h->readConnectivity(cells);
}
void HLLreadData(IOFile *handle,char *name,void *data){
  ReadHLL *h = (ReadHLL *)handle;
  h->readData(name,data);
}

