#include <stdio.h>
#include <stdlib.h>
#include "SockIOwriter.hh"

void main(int argc,char *argv[]){
  // Open a SockIOwriter using 512k TCP windowsize
  SockIOwriter *writer= new SockIOwriter("localhost",7052,512*1024);
  puts("begin\n");
  float data[16*16*16]; // send 64^3 data for performance tests
  int rank=3;
  int dims[3]={16,16,16};

  for(int i=0;i<16*16*16;i++) data[i]=(float)i;
  for(i=0;i<4;i++){
    fprintf(stderr,"Writing Dataset %u\n",i);
    fprintf(stderr,"Wrote %u characters\n",
	    writer->write(IObase::Float32,rank,dims,data));
    fprintf(stderr,"\tWrote Dataset %u\n",i);
    sleep(1);
  }
}
