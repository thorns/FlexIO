// .NAME vtkAMRStructuredPointsReader - read structured point data from AMR datafile.
// .SECTION Description
// vtkAMRStructuredPointsReader is a source object that reads AMR 
// data files. See text for format details.
// .SECTION Caveats
// Data stored in AMR format is binary (compact) and system independent.
// Information on AMR can be found at http://hdf.ncsa.uiuc.edu
// Information on HDFIO/IEEEIO can be found at http://bach.ncsa.uiuc.edu/IEEEIO

#ifndef __vtkAMRStructuredPointsReader_h
#define __vtkAMRStructuredPointsReader_h

#include "Brads_AmrFileReader.hh"
#include <vtkStructuredPointsSource.h>
#include <HDFIO.hh>
#include <IEEEIO.hh>
//#include <AmrFileReader.hh>

class VTK_EXPORT vtkAMRStructuredPointsReader : public vtkStructuredPointsSource
{
 public:
  
  
  vtkAMRStructuredPointsReader();
  int logged;
  float loggedmin,loggedmax;
  void SetLoggedOn(){logged=1;}
  void SetLoggedOff(){logged=0;}
  // will need to replace with vtkIntArray and vtkString to be safe!!
  ~vtkAMRStructuredPointsReader(){
    if(file) delete file; // close file if it is open
    if(skipmap) delete skipmap;
    if (myreader) delete myreader;
  }
  static vtkAMRStructuredPointsReader *New() {return new vtkAMRStructuredPointsReader;};
  const char *GetClassName() {return "vtkAMRStructuredPointsReader";}
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // overload because of vtkDataReader ivar
  virtual unsigned long GetMTime();

  void SetFileName(char *name);
  char *GetFileName(){return filename;}
  //----------Johns stuff
  
  int GetNumTimeSteps(){return maxtime-mintime;}
  int SelectTimeStep(int timestep);
  int GetNumLevels(){return maxlevel+1;};
  int SelectLevel(int level);
  int GetNumDatasets(){return maxset;};
  int SelectDataset(int dataset);
  Brads_AmrFileReader *myreader;
protected:
  void Execute();
  IObase *file;
  HDFIO *hdf_file; // for HDF/NetCDF specific features
  int curtime, curlevel, curset, index;
  int maxtime, mintime, maxlevel, maxset, maxindex;
  
  int *skipmap;
  char filename[256]; // static buffer for filename... maybe use newstring()
};

#endif

