#include <stdio.h>
#include <stdlib.h>
#include "IO.hh"
#include "IEEEIO.hh"

int main(int argc,char *argv[]){
  float data[40*40*40];
  double origin[3]={0,0,0};
  double dx[3]={0.25,0.25,0.25};
  int rank=3;
  int dims[3]={40,40,40};
  IObase *outfile = new IEEEIO("data3d.raw",IObase::Create);
  int i,k;
  for(i=0;i<40*40*40;i++)
    data[i]=(float)i;

  puts("reserving chunk for z-slicing");
  outfile->reserveChunk(IObase::Float32,rank,dims);
  for(i=0;i<40;i+=10){
    int cdims[3]={40,40,10};
    int corigin[3]={0,0,0};
    corigin[2]=i;
    outfile->writeChunk(cdims,corigin,data);
  }

  puts("reserving chunk for y-slicing");
  outfile->reserveChunk(IObase::Float32,rank,dims);
  for(i=0;i<40;i+=10){
    int cdims[3]={40,10,40};
    int corigin[3]={0,0,0};
    corigin[1]=i;
    outfile->writeChunk(cdims,corigin,data);
  }

  puts("reserving chunk for x-slicing");
  outfile->reserveChunk(IObase::Float32,rank,dims);
  for(i=0;i<40;i+=10){
    int cdims[3]={10,40,40};
    int corigin[3]={0,0,0};
    corigin[0]=i;
    outfile->writeChunk(cdims,corigin,data);
  }


  puts("reserving chunk for domain-block chunking (10x10x10 blocks)");
  for(int x=0;x<40;x+=10){
    int cdims[3]={10,10,10};
    for(int y=0;y<40;y+=10){
      for(int z=0;z<40;z+=10){
	int corigin[3];
	corigin[0]=x;
	corigin[1]=y;
	corigin[2]=z;
	outfile->writeChunk(cdims,corigin,data);
      }
    }
  }

  puts("done... deleting");
  delete outfile;
  puts("done");
  return 1;
}

