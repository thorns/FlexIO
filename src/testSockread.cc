#include <stdio.h>
#include <stdlib.h>
#include "SockIOreader.hh"

void main(int argc,char *argv[]){ 
  // Open a SockIOreader using 512k TCP windowsize
  SockIOreader *reader= new SockIOreader("scratch.raw",7052,512*1024);
  puts("begin\n");
  while(reader->nDatasets()<4){
    int ndsets=reader->nDatasets();
    fprintf(stderr,"reader->nDatasets()=%u\r",ndsets);
  }
  // now go through and verify each dataset
  for(int i=0;i<4;i++){ 
    float buffer[16*16*16];
    int rank,dims[3];
    IObase::DataType type;
    reader->seek(i); // seek to dataset 
    reader->readInfo(type,rank,dims);
    printf("Dataset[%u]: Rank=%u Dims={%u,%u,%u}\n",i,rank,dims[0],dims[1],dims[2]);
    reader->read(buffer);
    puts("Print out dataset info to verify correctness");
    for(int j=0;j<IObase::nElements(rank,dims);j+=100){
      float f=(float)j;
      printf("Data[%u]=%f\n",j,buffer[j]);
    }
  }
  puts("\ndone\n");
}
