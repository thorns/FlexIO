#include <FlexIO.hh>
#include <IEEEIO.hh>
#include <string.h>

#ifdef	WITH_HDF5
#include <H5IO.hh>
#endif

#ifdef	WITH_HDF4
#include <HDFIO.hh>
#endif

/*
  This check should be more advanced, of course.
  It currently just checks if the extension is anywhere in the
  filename. This might be ok in most situation, but will fail
  if the filename is e.g. /tmp/dir.h5/data.ieee ...
  Should check for true file extension (I'm too lazy here).
  Possibly it might be even better to read the first four bytes of
  the file - if it already exists - and determine the file type based
  on that.
 */

/*
In preparation for the automatic detection and opening of the proper file
type, IEEEIO was designed so that you can sense the file type using the 1st 4
bytes of the file (the magic number), so that you don't have to rely on the
file extensions.  HDF4 and HDF5 also use this same methodology.  IEEEIO's 
magic number is 0x01020304.  HDF4 uses the ascii representation for ctrl-n
ctrl-c ctrl-s ctrl-a (^N^C^S^A).  These are stored as individual characters
and so must be read consecutively from the file so as to counteract the
effects of byte-swapping.  In IEEEIO, I actually store the magic number as a
sequence of characters followed by the same magic number written as an
integer.  This is how IEEEIO auto-detects the byte-order of the file (or at
least whether the file's byte order is opposite that of the machine
architecture which is reading it).
  
-john    
*/
IObase*	FlexIOopen(const char*filename, IObase::AccessMode mode)
{

#ifdef	WITH_HDF5
	if (strstr(filename, ".h5") )
		return new H5IO(filename, mode);
#endif

#ifdef	WITH_HDF4
	if (strstr(filename, ".hdf") )
		return new HDFIO(filename, mode);
#endif

	return new IEEEIO(filename, mode);
}

