#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "IO.hh"
#include "IEEEIO.hh"
int main(int argc,char *argv[]){
  if(argc!=3){
    printf("Correct usage is %s <infile> <outfile>\n",argv[0]);
    exit(1); // failure code
  }
  IObase *infile=new IEEEIO(argv[1],IObase::Read);
  if(!infile->isValid()){printf("cant read %s\n",argv[1]); exit(0);}
  IObase *outfile=new IEEEIO(argv[2],IObase::Create);
  if(!outfile->isValid()){
    printf("cant write %s\n",argv[2]); 
    delete infile; 
    exit(0);
  }
  for(int i=0;i<infile->nDatasets();i++){
    int rank,dims[5];
    IObase::DataType type;
    char *data;

    fprintf(stderr,".%u",i);
    infile->readInfo(type,rank,dims);
    {
      int j,sz;
      for(j=0,sz=1;j<rank;j++) sz*=dims[j];
      sz*=IObase::sizeOf(type);
      data=new char[sz];
    }
    infile->read(data);
    outfile->write(type,rank,dims,data);
    delete data; // OK, so I'm not being smart about mem allocation here
    for(int j=0;j<infile->nAttributes();j++){
      char name[256]; // should be big enough
      int length;
      fprintf(stderr,"a");
      infile->readAttributeInfo(j,name,type,length);
      data = new char[length * IObase::sizeOf(type)];
      infile->readAttribute(j,data);
      outfile->writeAttribute(name,type,length,data);
      delete data;
    }
  }
  puts("");
  delete infile;
  delete outfile;
  return 0; // success code
}
