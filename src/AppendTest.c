#include <stdio.h>
#include "IEEEIO.h"

int main(int argc, char **argv) {
  IOFile noapp, app, appch;
  Int i,j,k;
  double x[20][20][20];
  double y[10][20][20];
  int dims[3],chdim[3],chori[3];

  dims[0] = 20; dims[1] = 20; dims[2] = 20;

  for (i=0;i<20;i++)
    for (j=0;j<20;j++)
      for (k=0;k<20;k++) {
	x[i][j][k] = 20*i + j + k*0.05;
	if (i < 10) y[i][j][k] = x[i][j][k];
      }

  /* OK so first write the one three times without appending */
  noapp = IEEEopen("noapp.ieee","w");
  for (i=0;i<3;i++) {
    IOwrite(noapp, FLOAT64, 3, dims, x);
  }
  IOclose(noapp);

  /* Now do the appended one */
  for (i=0;i<3;i++) {
    if (i == 0)
      app = IEEEopen("app.ieee","w");
    else
      app = IEEEopen("app.ieee","a");
    IOwrite(app, FLOAT64, 3, dims, x);
    IOclose(app);
  }

  /* Now do a chunked one */
  for (i=0;i<3;i++) {
    if (i == 0)
      appch = IEEEopen("appch.ieee","w");
    else
      appch = IEEEopen("appch.ieee","a");

    IOreserveChunk(appch,FLOAT64,3,dims);
    chdim[0] = 20;
    chdim[1] = 20;
    chdim[2] = 20;
    chori[0] = 0;
    chori[1] = 0;
    chori[2] = 0;
    IOwriteChunk(appch,chdim,chori,x);
    IOclose(appch);
  }

  /* Now do a chunked one without appending */
  appch = IEEEopen("noappch.ieee","w");
  for (i=0;i<3;i++) {
    IOreserveChunk(appch,FLOAT64,3,dims);
    chdim[0] = 20;
    chdim[1] = 20;
    chdim[2] = 20;
    chori[0] = 0;
    chori[1] = 0;
    chori[2] = 0;
    IOwriteChunk(appch,chdim,chori,x);
  }
  IOclose(appch);

}
