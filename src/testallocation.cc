#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FlexArrayTmpl.H"

struct InnerContainer {
  int num;

  InnerContainer(InnerContainer &src){
    this->num=src.num;
  }
  InnerContainer &operator=(InnerContainer &src){
    if(this==&src) return src;
    this->num=src.num;
    return *this;
  }
  InnerContainer(){
    printf("\tNew InnerContainer %p\n",this); 
    num=-1; /* uninitialized */  
  }
  ~InnerContainer(){
    printf("\tInnerContainer[%p] destruct %d\n",this,num);
  }
};

struct OuterContainer {
  int num;
  FlexArray<InnerContainer> inner;
  OuterContainer(OuterContainer &src){
    this->num=src.num;
    this->inner=src.inner;
  }
  OuterContainer &operator=(OuterContainer &src){
    if(this==&src) return src;
    this->num=src.num;
    this->inner=src.inner;
    return *this;
  }
  OuterContainer(){
    printf("New OuterContainer %lu\n",(unsigned long)this); 
    num=-1;
  }
  ~OuterContainer(){
    printf("OuterContainer[%lu] destruct %d innersize=%u\n",
	   (unsigned long)this,num,inner.getSize());
  }
};

struct Container {
  FlexArray<OuterContainer> outer;
	
  Container(int sz){
    int i,j;
    outer.setSize(sz);
    for(i=0;i<sz;i++){
      OuterContainer o;
      printf("***Outer create [%u]\n",i);
      o.num=i;
      o.inner.setSize(sz);
      for(j=0;j<sz;j++){
	printf("\t***Inner create [%u]\n",j);
	(o.inner[j]).num=j;
      }
      outer[i]=o;
    }
  }
	
  ~Container(){
    printf("Container Destructor outersize=%u\n",outer.getSize());
  }
};

void main(int argc,char *argv[]){
  Container *c;
  puts("new container");
  c = new Container(3);
  puts("CREATED--------(now destroy)");
  delete c;
  puts("DONE");
}

