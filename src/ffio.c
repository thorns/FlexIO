#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/times.h>
#include <sys/statfs.h>
#include <ffio.h>


#define MINBLOCKSIZE     	(1*1024)
#define MAXBLOCKSIZE     	(512*1024)	/* multiple of MINBLOCKSIZE */

#define MAXFILESIZE     (128*1024*1024)	/* multiple of MAXBLOCKSIZE */
#define DATAFILE     "bla"
#define STATFILE     "ffio.c"

#define	LAYERS		"" /*bufa.bufsize=1024.num_buffers=1"*/

#ifdef	T3E
#define	S_IWRITE	00200
#define	S_IREAD		00400
#endif


int main (int argc, char *argv [])
{
  int i, blocksize, rank=3;
  int outfile;
  void *data;
  struct tms starttime, stoptime;
  double utime, stime;
	int open_mode;
	int cblks;	/* size = cblks * 512 words */
	long cbits;
	char layers [256];
	struct ffsw ffopens_status;
#ifdef	T3E
	struct statfs statfs_info;
#endif


  data = (void *) calloc (sizeof (char), MAXBLOCKSIZE);
  if (! data) {
    printf ("Couldn't allocate enough memory\n");
    exit (1);
  }

	/* set file open parameters */
#ifdef	T3E
#if	0
	if (statfs (STATFILE, &statfs_info, sizeof (statfs_info), 0) < 0) {
		perror ("statfs() failed: ");
		exit (1);
	}
	printf ("secondary partitions' bitmask: 0x%lx\n", statfs_info.f_secparts);

	cbits = 0;
	cbits |= (1 << 24);
	cbits |= (1 << 06);
	cbits |= (1 << 26);
	cbits |= (1 << 23);
#endif
	open_mode = O_WRONLY | O_CREAT | O_PLACE;
	cbits = 0;
	cblks = 100;
#else
	open_mode = O_WRONLY | O_CREAT;
	cbits = 0;
	cblks = 0;
#endif

	for (blocksize = MINBLOCKSIZE; blocksize <= MAXBLOCKSIZE; blocksize *= 2) {

		times (&starttime);

		/* open the file */
		outfile = ffopens (DATAFILE, open_mode, S_IWRITE | S_IREAD,
							cbits, cblks, &ffopens_status, LAYERS);
		if (! outfile) {
			perror ("ffopens() failed: ");
			exit (1);
		}

		/*	system ("setf -n32768b:128b -c -p 24:06:23:26 bla");*/

		/* write some data */
		for (i = 0; i < MAXFILESIZE / blocksize; i++)
			if (ffwrite (outfile, data, blocksize) < 0) {
				perror ("ffwrite() failed: ");
				exit (1);
			}

		/* close and remove the file */
		ffclose (outfile);
		remove (DATAFILE);

		times (&stoptime);
		utime = (stoptime.tms_utime -starttime.tms_utime) / (double) CLK_TCK;
		stime = (stoptime.tms_stime -starttime.tms_stime) / (double) CLK_TCK;
		printf ("blocksize [bytes]: %d\ttimes(u/s) [s]: %f %f\tbandwidth [MB/s]: %f\n",
				blocksize, utime, stime,
				MAXFILESIZE / (utime + stime) / (1024*1024));
	}
	free (data);

	return (0);
}
