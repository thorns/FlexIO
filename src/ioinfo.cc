#include <stdio.h>
#include <stdlib.h>
#include "Arch.h"
#include "IO.hh"
#include "IEEEIO.hh"

#ifdef WITH_HDF4
#include "HDFIO.hh"
#endif
#ifdef WITH_HDF5
#include "H5IO.hh"
#endif

// dummy routine
extern "C" void CCTKi_RegisterThorn() {}

struct CmdlnParams {
  char *programname;
  char *filename,*filetype;
  int showdatasetinfo,showannotationinfo,showattribinfo;
  int showdatamax,showannotationmax,showattribmax;
  int showdatastats,showattribstats;

  void setDefaults(){
    programname="";
    filename=0;
    filetype=0;
    showdatasetinfo=1;
    showannotationinfo=0;
    showattribinfo=0;
    showdatamax=0;
    showannotationmax=0;
    showattribmax=0;
    showdatastats=0;
    showattribstats=0;
  }

  CmdlnParams(){
    setDefaults();
  }

  CmdlnParams(int argc,char *argv[]){
    setDefaults();
    parse(argc,argv);
  }

  void parse(int argc,char *argv[]){
    programname=argv[0];
    for(int i=1;i<argc;i++){
      char *str=argv[i];
      if(*str=='-'){
	char *val=argv[i+1];
	str++;
	//printf("read option[%u]=[%s]\n",i,str);
	if(!strcmp(str,"showdata"))      { 
	  puts("option -showdata:\tShow first 10 data entries");
	  showdatamax=10;}
	else if(!strcmp(str,"hidedata")) {
	  puts("option -hidedata:\tDon't show data");
	  showdatamax=0; }
	else if(!strncmp(str,"showann",7)&&
		strstr(str,"data")) {
	  puts("option -showanndata:\tShow annotation text (max of 10 chars)");
	  showannotationmax=10; showannotationinfo=1; }
	else if(!strncmp(str,"hideann",7)&&
		strstr(str,"data")) {
	  puts("option -hideanndata:\tSupress printing of annotation text");
	  showannotationmax=0; }
	else if(!strncmp(str,"showatt",7) &&
		strstr(str,"data"))  {
	  puts("option -showattdata:\tShow first 10 elements of attribute data");
	  showattribmax=10; 
	  showattribinfo=1;}	
	else if(!strncmp(str,"hideatt",7)&&
		strstr(str,"data")) {
	  puts("option -hideatt:\tHide attribute info");
	  showattribmax=0; }
	else if(!strncmp(str,"showann",7)&&
		strstr(str,"info")) {showannotationinfo=1;}
	else if(!strncmp(str,"hideann",7)&&
		strstr(str,"info")) {showannotationinfo=0; }
	else if(!strncmp(str,"showatt",7) &&
		strstr(str,"info"))  {
	  puts("option -showattinfo:\tShow attribute info (name,type,length)");
	  showattribinfo=1; }	
	else if(!strncmp(str,"hideatt",7) &&
		strstr(str,"info")) {showattribinfo=0; }
	else if(!strcmp(str,"file"))     {
	  filename=val;   
	  i++;
	  filetype=strrchr(str,'.') + 1;
	  printf("option -file %s: Read file named [%s]\n",filename,filetype);
	}
	else if(!strcmp(str,"type"))     {filetype=val;   i++;}
      }
      else { // its probably a filename
	filename=str;
	filetype=strrchr(str,'.');
	if(filetype) filetype++;       // skip the '.'
	else filetype = "";
      }
    }
  }
};

char *Typename(IObase::DataType datatype){
  char *typestring;
  switch(datatype){
  case IObase::Int8:
    typestring="Int8";
    break;
  case IObase::Int16:
    typestring="Int16";
    break;
  case IObase::Int32:
    typestring="Int32";
    break;
  case IObase::Int64:
    typestring="Int64";
    break;
  case IObase::Float32:
    typestring="Float32";
    break;
  case IObase::Float64:
    typestring="Float64";
    break;
  case IObase::Char8:
    typestring="String";
    break;
  default:
    typestring="<Unknown>";
    break;
  }
  return typestring;
}

int main(int argc,char *argv[]){
  CmdlnParams cmdln(argc,argv); // parse commandline parameters & flags
  if(!cmdln.filename) {
    puts("HEY!  Need a filename there buddy!");
    exit(1); // failure code
  }

  int i;
  IObase *infile = NULL;

  // Figure out what kind of file to open as
#ifdef WITH_HDF5
  if(!strcmp(cmdln.filetype,"h5")){
    infile = new H5IO(cmdln.filename,IObase::Read);
  }
#endif
#ifdef WITH_HDF4
  if(!strcmp(cmdln.filetype,"hdf")){
    infile = new HDFIO(cmdln.filename,IObase::Read);
  }
#endif
  if(!strcmp(cmdln.filetype,"raw") || !strcmp(cmdln.filetype,"ieee")){
    infile = new IEEEIO(cmdln.filename,IObase::Read);
  }
  if(!infile){
    puts("I don't recognize that type of file, but I'll try to open as ieee");
    cmdln.filetype="ieee";
    infile = new IEEEIO(cmdln.filename,IObase::Read);
  }
  if(!infile->isValid()){
    printf("%s: The file %s is not a valid %s file... sorry\n",
	   cmdln.programname,cmdln.filename,cmdln.filetype);
    exit(1);
  }
  int ndatasets=infile->nDatasets();
  printf("Number of datasets=%d\n",ndatasets);
  puts  ("===========================");

  // OK... now its time for da main event loop....
  for(i=0;i<ndatasets;i++){
    int j;
    int rank,dims[5];
    IObase::DataType datatype;

    printf("Dataset[%u]\n",i);
    puts("--------------");
    infile->readInfo(datatype,rank,dims);
    
    printf("Datatype=%s Rank=%u Dims=",Typename(datatype),rank);
    for(j=0;j<rank-1;j++)
      printf("%u,",dims[j]);
    printf("%u\n",dims[rank-1]); 
#if 0 // blow this away for now
    if(cmdln.showdatamax){// || cmdln.showdatastats (not implemented yet)
      // must allocate memory for it
      void *buffer=0;
      int *idims=new int[rank];
      for(int dim=0;dim<rank;dim++) idims[dim]=0;
      idim[0]=showdatamax;
      if(IObase::sizeOf(datatype)>0 && dims[0]>0)
	buffer=new char[idims[0] * IObase::sizeOf(datatype)];
      //infile->readChunk(buffer);
      delete buffer;
      delete idims;
    }
#endif
    int nattribs=infile->nAttributes();
    int nannotations=infile->nAnnotations();
    printf("Annotations: %u\n",nannotations);
    printf("Attributes:  %u\n",nattribs);

    if(cmdln.showattribinfo){
      for(j=0;j<nattribs;j++){
	char attribname[128];
	Long length;
	infile->readAttributeInfo(j,attribname,datatype,length);
	printf("\tAttribute[%u] Name=%s Type=%s Length=%u\n",
	       j,attribname,Typename(datatype),length);
	if(cmdln.showattribmax || cmdln.showattribstats){
	//printf("Length of Attribute=%u for total bytes=%u\n",length,IObase::sizeOf(datatype));
	  void *dataptr;
	  dataptr=0;
	  if(IObase::sizeOf(datatype)>0 && length>0)
	    dataptr=new char[length * IObase::sizeOf(datatype)];
	  else {
	    //puts("no data to display");
	    continue; // no data to display
	  }
	  
	  infile->readAttribute(j,dataptr);
	  switch(datatype){
	  case IObase::Char8:{
	    char *d=(char*)dataptr;
	    d[length]='\0';
	    //puts("show char data");
	    printf("\t%s string[%u]:= %s\n",attribname,length,d);
	  }break;
	  case IObase::Int8:
	  case IObase::uInt8:{
	    char *d=(char*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      if(d[k]>=0x20 && d[k]<=0x7e)
		printf("\t%s[%u]:=%c\n",attribname,k,d[k]);
	      else {
		int intchar=(unsigned int)(d[k]);
		printf("\t%s[%u]:=%u\n",attribname,k,intchar);
	      }
	  }break;
	  case IObase::Int16:{
	    short *d=(short*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      printf("\t%s[%u]:=%u\n",attribname,k,d[k]);
	  }break;
	  case IObase::Int32:{
	    int *d=(int*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      printf("\t%s[%u]:=%u\n",attribname,k,d[k]);
	  }break;
	  case IObase::Int64:{
	    Long8 *d=(Long8*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      printf("\t%s[%u]:=%lu\n",attribname,k,(long unsigned int)d[k]);
	  }break;
	  case IObase::Float32:{
	    float *d=(float*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      printf("\t%s[%u]:=%f\n",attribname,k,d[k]);
	  }break;
	  case IObase::Float64:{
	    double *d=(double*)dataptr;
	    for(int k=0;k<cmdln.showattribmax && k<length;k++)
	      printf("\t%s[%u]:=%f\n",attribname,k,d[k]);
	  }break;
	  default:
	    puts("-----<nil>-----");
	    break;
	  }
	  if(dataptr)
	    delete (char*)dataptr;
	}
      }
    }
  }
  puts("Closing file");
  delete infile;
  puts("***done");
  return 0;
}
