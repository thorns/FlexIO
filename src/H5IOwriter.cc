/*  
 *  This example writes data to the HDF5 file.
 *  Data conversion is performed during write operation.  
 */
#include <H5IO.hh>

/* Some macros to fix compatibility issues as long as 1.8.0 is in beta
   phase */
#define H5_USE_16_API 1
#include <hdf5.h>

#define FILE        "SDS.h5"
#define DATASETNAME "IntArray" 
#define NX     5                      /* dataset dimensions */
#define NY     6
#define RANK   2
/*
Perhaps support a filtration/pattern matching architecture which
recognizes particular patterns of values in the file and offers those
patterns as a new typename in the architecture or even a particular
callback.  So for instance you could have a group of "dataset" with
"edgecoords" is a rectilinear dataset.  Otherwise we'll need to 
match groupnames?  Could have a "type" attribute which disambiguates
this because each object must have a unique text name.
 */

main (void)
{                         
  // int data[NX][NY];          /* data to write */
  int *data = new int[NX*NY];
  int i, j;
  int idx,rval;
  int dims[3];
  int rank=RANK;
  
  /* 
   * Data  and output buffer initialization. 
   */
  for (j = 0; j < NX; j++) {
    for (i = 0; i < NY; i++)
      data[j*NY + i] = i + j;
  }     
  /*
   * 0 1 2 3 4 5 
   * 1 2 3 4 5 6
   * 2 3 4 5 6 7
   * 3 4 5 6 7 8
   * 4 5 6 7 8 9
   */
  
  /*
   * Create a new file using H5F_ACC_TRUNC access,
   * default file creation properties, and default file
   * access properties.
   */
  H5IO *file = new H5IO(FILE,IObase::Write);
  /*
   * Describe the size of the array and create the data space for fixed
   * size dataset. 
   */
  dims[0] = NX;
  dims[1] = NY;
  float attr[3]={1.0,2.0,3.0};
  file->write(IObase::Int32,2,dims,data);
  file->writeAttribute("floatattr",IObase::Float32,3,attr);
  file->write(IObase::Int32,2,dims,data);
  delete file;

  puts("**********************Now read the file*******************");
  /* typedef herr_t *(H5G_operator_t)(hid_t group_id, 
     const char *member_name, 
     void *operator_data     // in,out); */
 file = new H5IO(FILE,IObase::Read);
 int nds = file->nDatasets();
 printf("number of datasets = %u\n",nds);
 for(int i=0;i<nds;i++){
   char name[32];
   IObase::DataType datatype;
   int rank,dims[3];
   file->readInfo(name,datatype,rank,dims);
   printf("name = [%s] rank=%u dims=%u,%u\n",name,rank,dims[0],dims[1]);
   if(datatype==IObase::Int32) puts("\tcorrect type Int32\n");
   else puts("\ttype failed");
   printf("\tnattribs=%u\n",file->nAttributes());
 }
 delete file;
 return 0;
}
