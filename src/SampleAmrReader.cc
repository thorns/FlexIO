#include <stdio.h>
#include <stdlib.h>
#include <IEEEIO.hh>
#include "AmrFileReader.hh"

int main(int argc,char *argv[]){
	IEEEIO *file;
	AmrFileReader *amrfile;

	if (argc < 2) {
		fprintf(stderr,"Usage: SampleAmrReader AMRfile\n");
		exit(0);
	}

	printf("Open the file named %s\n\n",argv[1]);
	file = new IEEEIO(argv[1],IObase::Read);

	if (!file->isValid()) { 
		fprintf(stderr,"Couldn't open %s for reading\n",argv[1]); 
		exit(0);
	}

	printf("Now we put the file into an AMR parsing framework\n\n");
	amrfile = new AmrFileReader(*file);
	amrfile->setDataLoadingOff();

	printf("Number of Levels = %u\n\n",amrfile->nLevels());

	int min,max;
	amrfile->getTimeRange(min,max);

	printf("The time range of AMR data in this file\n  (in terms of finest-level-of-resolution timesteps) = %u:%u\n\n",min,max);
	printf("Lets tell the driver to load up all of the grids for the first timestep\n\n");

	amrfile->showAllLevels();
	amrfile->setTime(min);

	printf("========================> ActiveGrids are (all levels)...\n");
	amrfile->printActiveGrids();
	// printf("========================> Grid Info is...\n");
	// amrfile->printGridInfo();

	printf("\nLet's hide level %d\n",amrfile->nLevels() - 2);
	amrfile->hideLevel(amrfile->nLevels() - 2);
	printf("Let's hide level %d\n",amrfile->nLevels() - 1);
	amrfile->hideLevel(amrfile->nLevels() - 1);
	printf("Tell the driver to load up the active grids for the first timestep\n");
	amrfile->setTime(min);
	printf("========================> ActiveGrids are (all but finest level)...\n");
	amrfile->printActiveGrids();

	printf("\nActually load the grid data into %u standard arrays\n",amrfile->getNumGrids());
	AmrGrid *grids = new AmrGrid[amrfile->getNumGrids()];
	amrfile->getGrids(grids); // that loaded all of the data 

	printf("We read %u grids\n\n",amrfile->getNumGrids());

	printf("Let's grab the grids into a flexarray (a dynamically resizable array)\n");
	FlexArray<AmrGrid> gridarray;
	amrfile->getGrids(gridarray);

	printf("We read %u grids\n",gridarray.getSize());

	// You can access individual grids with gridarray[]

	// Take a look at AmrGrid.hh to see whats in the datastruct
	// or take a look at docs 
	// http://infinite-entropy.lbl.gov/FlexIO/AmrGrid.html

	// To get rid of the data associated with these grids we can do a
	// amrfile->setDataLoadingOff(); // this deletes the data arrays
	// amrfile->setDataLoadingOn(); // reloads the data arrays from disk

	return 1;
}
