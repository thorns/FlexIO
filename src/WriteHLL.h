
#ifndef __HLL_WRITER_H_
#define __HLL_WRITER_H_

IOFile *HLLnewWriter(IOFile *file);
void HLLdeleteWriter(IOFile *file);

void HLLsetCoordType(IOFile *handle,int coordtype);
void HLLsetIndexType(IOFile *handle,int indextype);
void HLLnewDataset(IOFile *handle,int ndims,long npoints);
void HLLwriteCoords(IOFile *handle,
		 void *xcoords,void *ycoords,void *zcoords); 
void HLLwriteCoordsXYZ(IOFile *handle,void *xcoords);
void HLLwriteConnectivity(IOFile *handle,void *cells,long ncells);
void HLLwriteData(IOFile *handle,char *dataname,int datatype,
		 void *data,int veclen);

IOFile *HLLnewReader(IOFile *base);
void HLLdeleteReader(IOFile *handle);

void HLLselectDataset(IOFile *handle,
		      int index,int *ndims,int *npoints,
		      int *ncells,int *ndata);
void HLLreadDataInfo(IOFile *handle,char *names[],
		     int *datatypes,int *veclens);
void HLLreadCoords(IOFile *handle,
		   float *xcoords,float *ycoords,float *zcoords);
void HLLreadConnectivity(IOFile *handle,int *cells);
void HLLreadData(IOFile *handle,char *name,void *data);


#endif
