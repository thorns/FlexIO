/////////////////////////////////////////////////////////////////
//
// $Id$
//
// $Log$
// Revision 1.1.1.1  2000/09/13 13:49:12  tradke
// Importing latest stuff from development repository
//
// Revision 1.1  2000/04/28 08:51:16  werner
// Wrapper function for runtime detection of IEEEIO,HDF5,HDF4 format.
// Takes a filename and returns an IObase* object.
// For compilation, the flags -DWITH_HDF5  and -DWITH_HDF4 have to be set
// to enable HDF{4|5} functionality, otherwise just IEEEIO is available.
// These flags have to be set in the makefile, depending on wether the HDF{4|5}
// libraries are available at compilation time.
//
// Created 2000/04/28 10:28:21  werner
// Initial version
//
//
/////////////////////////////////////////////////////////////////
#ifndef __FLEXIO_HPP
#define __FLEXIO_HPP "Created 28.04.2000 10:28:21 by werner"

#include "IO.hh"

IObase*	FlexIOopen(const char*filename, IObase::AccessMode = IObase::Read);


#endif /* __FLEXIO_HPP */
