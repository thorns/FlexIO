
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IEEEIO.hh>
#include "AMRwriter.hh"
typedef char *charp;
struct CommandLine {
  char *programname;
  charp infile[32];
  char outfile[128];
  int maxlevel;
  
  void setDefaults(){
    programname="";
    for(int i=0;i<32;i++) infile[i]=0;
    strcpy(outfile,"amrout.ieee");
    maxlevel=0;
  }

  CommandLine(){
    setDefaults();
  }

  CommandLine(int argc,char *argv[]){
    setDefaults();
    parse(argc,argv);
  }

  void parse(int argc,char *argv[]){
    programname=argv[0];
    for(int i=1;i<argc;i++){
      char *str=argv[i];
      if(*str=='-'){
	char *val=argv[i+1];
	str++;
	//printf("read option[%u]=[%s]\n",i,str);
	if(*str=='i')      { 
	  puts("option -i: input files");
	  i++;
	  for(maxlevel=0;i<argc && 
		(*(argv[i])!='-') && 
		maxlevel<32;
	      maxlevel++,i++)
	    infile[maxlevel]=argv[i];
	  i--;
	}
	else if(*str=='o') {
	  puts("option -o: output files");
	  strcpy(outfile,val);
	}
      }
    }
    if(!maxlevel) {
      usage();
      exit(0);
    }
  }
  void usage(){
    printf("Incorrect usage.  You should use\n");
    printf("%s -i <infile Level0> <infile Level1> ...<infile LevelN> -o <outfile>\n",programname);
    puts("Files must appear in order as they are read in.");
  }
};

int main(int argc, char *argv[]){
  CommandLine cmdln(argc,argv);
  typedef IEEEIO *IEEEIOp;
  int ninfiles;
  IEEEIOp infid[32],outfid;
  AMRwriter *amrfile;
  int i;
  
  for(i=0;i<cmdln.maxlevel;i++){
    infid[i]=new IEEEIO(cmdln.infile[i],IObase::Read);
    if(!infid[i]->isValid()) {
      printf("file %s is not an IEEEIO file\n",cmdln.infile[i]);
      exit(0);
    }
    else
      printf("Opening file %s as level %u for reading\n",
	     cmdln.infile[i],i);
  }
  outfid = new IEEEIO(cmdln.outfile,IObase::Write);
  if(!outfid->isValid()) {
    printf("file %s could not be opened for writing\n",cmdln.outfile);
    exit(0);
  }
  amrfile = new AMRwriter(*outfid);
  printf("Opened file %s for output\n",cmdln.outfile);
  
  // lets get info about the toplevel grid
  IObase::DataType type;
  int rank,dims[3];
  int nelements,attribnum;
  double delta[3],timestep,origin[3];

  infid[0]->seek(0);
  infid[0]->readInfo(type,rank,dims);
  amrfile->setType(type);
  
  // read delta (spacing between grid points at toplevel grid)
  attribnum=infid[0]->readAttributeInfo("delta",type,nelements);
  infid[0]->readAttribute(attribnum,delta);
  // read timestep size (in floating point realtime)
  //attribnum=infid[0]->readAttributeInfo("",type,nelements);
  //infid[0]->readAttribute(attribnum,timestep);
  timestep=1.0;
  // read origin (floating point origin).
  attribnum=infid[0]->readAttributeInfo("origin",type,nelements);
  infid[0]->readAttribute(attribnum,origin);

  printf("Setting toplevel params origin[%lf,%lf,%lf],Timestep[%lf],delta[%lf,%lf,%lf],maxlevel[%u]\n",
	 origin[0],origin[1],origin[2],
	 timestep,
	 delta[0],delta[1],delta[2],
	 cmdln.maxlevel);
  amrfile->setTopLevelParameters(rank,origin,delta,timestep,cmdln.maxlevel);
  amrfile->setRefinement(1, // timerefinement of 1 because we currently
			 // don't save intermedite timesteps.  In the future
			 // this would be 2 if you dump the intermediate
			 // timesteps
			 2 // spatial refinement between levels is 2 in each
			 // dimension
			 );
  // now we read all of the data from the input files
  // and write them out into the AMR file
  for(int n=0,ndatasets=infid[0]->nDatasets();n<ndatasets;n++){
    printf("Reading step %u\n",n);
    for(int level=0;level<cmdln.maxlevel;level++){
      printf("\tReading Level %u\n",level);
      char *data;
      infid[level]->seek(n); // make everyone seek to the same place
      amrfile->setTime(n);
      // this is assuming that all levels are dumped at the same time
      // rather than dumping them at the rate which they are computed.
      // (eg. this is a snapshot of the entire heirarchy at each timestep).
      // however this library can handle each level stepping at a different
      // rate.
      printf("Readinfo on %u\n",n);
      infid[level]->readInfo(type,rank,dims); // read in the data from src file
      printf("Info is type(%u) rank(%u) dims[%u,%u,%u]\n",
	     type,rank,dims[0],dims[1],dims[2]);
      data = new char[IObase::nBytes(type,rank,dims)]; // allocate space
      infid[level]->read(data); // read the actual data in
      amrfile->setLevel(level); // set the level to write to in amr output file
      // read the floating point origin attribute
      attribnum=infid[level]->readAttributeInfo("origin",type,nelements);
      infid[level]->readAttribute(attribnum,origin); // read it
      // write out the data to the AMR file
      amrfile->write(origin,dims,data); // write the data out
      delete data; // free the data storage
    }
  }
}
