#include <stdio.h>
#include <stdlib.h>
#include "IEEEIO.hh"

void main(int argc,char *argv[]){
  int i;
  float bogusdata[16*16];
  //float attribdata[128];
  int bogusdims[2]={16,16};
  int bogusrank=2;
  IO *outfile = new IEEEIO("testfile.raw",IO::Create);
  if(!outfile->isValid()){
    perror("IO open failed");
    exit(0);
  }
  else puts("Writing File");
  for(i=0;i<16*16;i++)
    bogusdata[i]=(float)i;
  char *string="I am annotation[0]";
  char *name="attrib0";
  for(i=0;i<5;i++){
    outfile->write(IO::Float32,bogusrank,bogusdims,bogusdata);
    (string[16])='0';
    for(int j=0;j<i;j++,(string[16])='0'+j)
      outfile->writeAnnotation(string);
    name[6]='0';
    //if(i<4)
      for(j=0;j<i;j++,name[6]='0'+j)
	outfile->writeAttribute(name,IO::Float32,2*j,bogusdata+j);
    
  }
  delete outfile;
  puts("##################REOPEN#######################");
  outfile = new IEEEIO("testfile.raw",IO::Read);

  if(!outfile->isValid()){
    perror("IO open failed");
    exit(0);
  }
  for(i=0;i<10;i++){
    IO::DataType dt;
    int idt;
    int rank,dims[3];
    for(int j=0;j<16*16;j++) bogusdata[j]=0.0; // clear bogusdata
    if(outfile->readInfo(dt,rank,dims)<=0){
      puts("***********************end****************");
      break;
    }
    outfile->read(bogusdata);
    printf("----------------------Data Read[%u]-------------------\n",i);
    idt=dt;
    printf("\tDatatype=%u rank=%u\n",idt,rank);
    for(j=0;j<3;j++) printf("\tDims[%u]=%u\n",j,dims[j]);
    puts("+++++++Annotations");
    for(j=0;j<outfile->nAnnotations();j++){
      char buffer[128];
      outfile->readAnnotation(j,buffer,sizeof(buffer));
      printf("\tAnnotation[%u]=[%s]\n",j,buffer);
    }
    puts("");
    for(j=0;j<16*16;j++) printf("data[%4u]=%f\n",j,bogusdata[j]);
  }
  printf("Seek=%d\n",outfile->seek(3));
  for(i=0;i<10;i++){
    IO::DataType dt;
    int idt;
    int rank,dims[3];
    for(int j=0;j<16*16;j++) bogusdata[j]=0.0; // clear bogusdata
    if(outfile->readInfo(dt,rank,dims)<=0){
      puts("***********************end****************");
      break;
    }
    outfile->read(bogusdata);
    printf("----------------------Data Read[%u]-------------------\n",i);
    idt=dt;
    printf("\tDatatype=%u rank=%u\n",idt,rank);
    for(j=0;j<3;j++) printf("\tDims[%u]=%u\n",j,dims[j]);
    puts("+++++++Annotations");
    for(j=0;j<outfile->nAnnotations();j++){
      char buffer[128];
      outfile->readAnnotation(j,buffer,sizeof(buffer));
      printf("\tAnnotation[%u]=[%s]\n",j,buffer);
    }
    puts("xxxxxxxAttribs");
    int buflen;
    IO::DataType attribtype;
    for(j=0;j<outfile->nAttributes();j++){
      float buffer[128];
      char namebuf[128];
      IO::DataType dt;
      outfile->readAttributeInfo(j,namebuf,dt,buflen);
      printf("\tAttribname[%u] = [%s]\n",j,namebuf);
      outfile->readAttribute(j,buffer);
      for(int k=0;k<buflen;k++)
	printf("\tattr data[%u]=%f\n",k,buffer[k]);
    }
    int i;
    if((i=outfile->readAttributeInfo("attrib2",attribtype,buflen))>=0){
      float buffer[128];
      outfile->readAttribute(i,buffer);
      puts("&&&&&Read [attrib2]");
      for(int k=0;k<buflen;k++)
	printf("\tattr data[%u]=%f\n",k,buffer[k]);
    }
    else puts("&&&&There is no [attrib2]");
				 
    puts("");
    for(j=0;j<16*16;j++) printf("data[%4u]=%f\n",j,bogusdata[j]);
  }
  delete outfile;
}

